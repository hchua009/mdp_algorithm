package com.mdp.simulation;

import java.util.Arrays;
import java.util.LinkedList;

public class FastestPath {
	
	private static final int WIDTH = 15;
	private static final int HEIGHT = 20;
	
	private Graph graph;
	
	private Arena arena;
	private boolean[][] obstacles; // 0 for free, 1 for obstacle
	
	public FastestPath(Arena arena, boolean[][] obstacles) {
		this.arena = arena;
		this.obstacles = obstacles;
		
		constructGraph();
	}
	
//	private Graph constructGraph() {
//		Graph graph = new Graph(WIDTH * HEIGHT);
//		for (int y = 1; y < HEIGHT; y++) {
//			for (int x = 1; x < WIDTH; x++) {
//				if (isValidRobotPosition(x, y)) {
//					int start = coordToNode(x, y);
//					
//					if (isValidRobotPosition(x - 1, y)) graph.addEdge(start, coordToNode(x - 1, y), 1);
//					if (isValidRobotPosition(x, y + 1)) graph.addEdge(start, coordToNode(x, y + 1), 1);
//					if (isValidRobotPosition(x + 1, y)) graph.addEdge(start, coordToNode(x + 1, y), 1);
//					if (isValidRobotPosition(x, y - 1)) graph.addEdge(start, coordToNode(x, y - 1), 1);
//					
//				}
//			}
//		}
//		moveRobot(optimizedShortestPath(graph, coordToNode(1, 1), coordToNode(14, 19)));
//		return null;
//	}
	
	private void constructGraph() {
		graph = new Graph(WIDTH * HEIGHT);
		for (int y = 1; y < HEIGHT - 1; y++) {
			for (int x = 1; x < WIDTH - 1; x++) {
				
				if (!isValidRobotPosition(x, y)) continue;
				
				// Go left
				for (int c = x - 1; c > 0; c--) {
					if (isValidRobotPosition(c, y)) {
						int dist = Math.abs(c - x);
						graph.addEdge(coordToNode(x, y), coordToNode(c, y), 0.99 * dist + 0.01);
					} else
						break;
				}
				
				// Go right
				for (int c = x + 1; c < WIDTH - 1; c++) {
					if (isValidRobotPosition(c, y)) {
						int dist = Math.abs(c - x);
						graph.addEdge(coordToNode(x, y), coordToNode(c, y), 0.99 * dist + 0.01);
					} else
						break;
				}
				
				// Go up
				for (int c = y + 1; c < HEIGHT - 1; c++) {
					if (isValidRobotPosition(x, c)) {
						double dist = Math.abs(c - y);
						graph.addEdge(coordToNode(x, y), coordToNode(x, c), 0.99 * dist + 0.01);
					} else
						break;
				}
				
				// Go down
				for (int c = y - 1; c > 0; c--) {
					if (isValidRobotPosition(x, c)) {
						int dist = Math.abs(c - y);
						graph.addEdge(coordToNode(x, y), coordToNode(x, c), 0.99 * dist + 0.01);
					} else
						break;
				}
				
			}
		}
	}
	
	LinkedList<int[]> fastestPath(int start, int end) {
		boolean[] inTree = new boolean[graph.numVertices];
		double[] distance = new double[graph.numVertices];
		double[] heuristic = new double[graph.numVertices];
		
		int[] parent = new int[graph.numVertices];
		
		Arrays.fill(parent, -1);
		Arrays.fill(distance, Double.MAX_VALUE);
		Arrays.fill(heuristic, 0);
		distance[start] = 0;
		
		int node = start;
		
		while(!inTree[node] && node != end) {
			inTree[node] = true;
			
//			if (parent[node] != -1) {
//				int[] parentCoord = nodeToCoord(parent[node]);
//				int[] childCoord = nodeToCoord(node);
//				int childX = childCoord[0], childY = childCoord[1];
//				if (parentCoord[1] == childY) {
//					if (isValidRobotPosition(childX + 1, childY)) graph.changeWeight(node, coordToNode(childX + 1, childY), 1);
//					if (isValidRobotPosition(childX - 1, childY)) graph.changeWeight(node, coordToNode(childX - 1, childY), 1);
//					if (isValidRobotPosition(childX, childY - 1)) graph.changeWeight(node, coordToNode(childX, childY - 1), 2);
//					if (isValidRobotPosition(childX, childY + 1)) graph.changeWeight(node, coordToNode(childX, childY + 1), 2);
//				} else {
//					if (isValidRobotPosition(childX + 1, childY)) graph.changeWeight(node, coordToNode(childX + 1, childY), 2);
//					if (isValidRobotPosition(childX - 1, childY)) graph.changeWeight(node, coordToNode(childX - 1, childY), 2);
//					if (isValidRobotPosition(childX, childY - 1)) graph.changeWeight(node, coordToNode(childX, childY - 1), 1);
//					if (isValidRobotPosition(childX, childY + 1)) graph.changeWeight(node, coordToNode(childX, childY + 1), 1);
//				}
//			}
			
			
			for (Edge edge : graph.nodes[node].edges) {
				int y = edge.y;
				double weight = edge.weight;
				
				
				if(!inTree[y] && (distance[y] > distance[node] + weight)) {
					parent[y] = node;
					distance[y] = distance[node] + weight;
				}
			}
			double min = Double.MAX_VALUE;
			for (int i = 0; i < graph.numVertices; i++) {
				double fValue = distance[i] + heuristic[i];
				if (!inTree[i] && fValue < min) {
					min = fValue;
					node = i;
				}
			}
			
		}
		
		LinkedList<int[]> path = new LinkedList<>();
		node = end;
		while (node != -1) {
			path.addFirst(nodeToCoord(node));
			node = parent[node];
		}
		
		return path;
	}
	
	LinkedList<int[]> fastestPath(int xStart, int yStart, int xEnd, int yEnd) {
		return fastestPath(coordToNode(xStart, yStart), coordToNode(xEnd, yEnd));
	}
	
	private LinkedList<int[]> expandPath(LinkedList<int[]> path) {
		LinkedList<int[]> newPath = new LinkedList<>();
		int[] prevCoord = path.pollFirst();
		newPath.addLast(prevCoord);
		
		while(!path.isEmpty()) {
			int[] coord = path.pollFirst();
			if (coord[0] == prevCoord[0]) {
				if (coord[1] > prevCoord[1]) {
					for (int i = prevCoord[1] + 1; i < coord[1]; i++) newPath.addLast(new int[]{coord[0], i});
					newPath.addLast(coord);
				} else {
					for (int i = prevCoord[1] - 1; i > coord[1]; i--) newPath.addLast(new int[]{coord[0], i});
					newPath.addLast(coord);
				}
			} else {
				if (coord[0] > prevCoord[0]) {
					for (int i = prevCoord[0] + 1; i < coord[0]; i++) newPath.addLast(new int[]{i, coord[1]});
					newPath.addLast(coord);
				} else {
					for (int i = prevCoord[0] - 1; i > coord[0]; i--) newPath.addLast(new int[]{i, coord[1]});
					newPath.addLast(coord);
				}
			}
			prevCoord = coord;
		}
		return newPath;
	}
	
	LinkedList<int[]> fastestExpandedPath(int xStart, int yStart, int xEnd, int yEnd) {
		return expandPath(fastestPath(coordToNode(xStart, yStart), coordToNode(xEnd, yEnd)));
	}
	
	
	
	void moveRobot(LinkedList<int[]> path) {
		
		
		LinkedList<int[]> finalPath = expandPath(path);
		System.out.println("Final path");
		System.out.println(finalPath);
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				arena.setFakeRobotDirection(arena.getRobotDirection());
				String fullMovementString="";
				int[] prevCoord = null;
				while(!finalPath.isEmpty()) {
					int[] coord = finalPath.pollFirst();
					boolean shouldTurnLeft = false, shouldTurnRight = false, shouldGoForward = true;
					if (prevCoord == null){						
						arena.setRobotPosition(coord[0], coord[1]);
					}
					else {
						String direction = arena.getFakeRobotDirection();
						if (direction.equals("NORTH")) {
							if (prevCoord[0] < coord[0]) shouldTurnRight = true;
							else if (prevCoord[0] > coord[0]) shouldTurnLeft = true;
							else if (prevCoord[1] > coord[1]) shouldGoForward = false;
							
						} else if (direction.equals("SOUTH")) {
							if (prevCoord[0] < coord[0]) shouldTurnLeft = true;
							else if (prevCoord[0] > coord[0]) shouldTurnRight = true;
							else if (prevCoord[1] < coord[1]) shouldGoForward = false;
							
						} else if (direction.equals("EAST")) {
							if (prevCoord[1] < coord[1]) shouldTurnLeft = true;
							else if (prevCoord[1] > coord[1]) shouldTurnRight = true;
							else if (prevCoord[0] > coord[0]) shouldGoForward = false;
							
						} else if (direction.equals("WEST")) {
							if (prevCoord[1] < coord[1]) shouldTurnRight = true;
							else if (prevCoord[1] > coord[1]) shouldTurnLeft = true;
							else if (prevCoord[0] < coord[0]) shouldGoForward = false;
						}
						
						if (shouldTurnLeft) {							
							arena.fakeTurnLeft();
							fullMovementString+="l";
							finalPath.addFirst(coord);
						} else if (shouldTurnRight) {							
							arena.fakeTurnRight();
							fullMovementString+="r";
							finalPath.addFirst(coord);
						} else {
							if (shouldGoForward) {									
								fullMovementString+="f";
							} else {
								fullMovementString+="b";
							}
						}
					}
					
					prevCoord = coord;
					
					
				}// end of while 
				
				
				String[] encodedStringArray=encode(fullMovementString);
				
				System.out.println("Encoded string: " + Arrays.toString(encodedStringArray));
				
				char movementChar;
				int movementNum;
				
				
				
				
				Client client=null;
				String latestMessageFromRpi=null;
				int timeToSleep=50;
				if (arena.getIsRealArena())
					client=arena.getClient();
				for (int i=0; i<encodedStringArray.length;i++){
					movementChar=encodedStringArray[i].charAt(0);
					
					movementNum=Integer.parseInt(encodedStringArray[i].substring(1));
					
					
					if (movementChar=='f'){
						for (int j=0; j<movementNum; j++){
							arena.moveForward();
						
							try {
								Thread.sleep( timeToSleep);
							} catch (Exception e) {				
								e.printStackTrace();
							}
							
						}
					}
					else if (movementChar=='b'){
						for (int j=0; j<movementNum; j++){
							arena.moveBackward();
							
							try {
								Thread.sleep( timeToSleep);
							} catch (Exception e) {				
								e.printStackTrace();
							}
							
						}
					}
					else if (movementChar=='l'){
						for (int j=0; j<movementNum; j++){
							arena.turnLeft();
							
							try {
								Thread.sleep( timeToSleep);
							} catch (Exception e) {				
								e.printStackTrace();
							}
						
						}
					}
					else if (movementChar=='r'){
						for (int j=0; j<movementNum; j++){
							arena.turnRight();
							
							try {
								Thread.sleep( timeToSleep );
							} catch (Exception e) {				
								e.printStackTrace();
							}
						
						}
					}
							
					
		
					if (arena.getIsRealArena()){
					
						if(movementNum<14){
							
									
							client.sendMessage(movementChar,movementNum);
							latestMessageFromRpi=client.receiveMessage();
							
							while(!  (latestMessageFromRpi.equals("Done"))){
								System.out.println("enter while loop of message !=Done ");
								latestMessageFromRpi=client.receiveMessage();
							}
						}
						else{
							
				
							client.sendMessage(movementChar,movementNum/2);
							latestMessageFromRpi=client.receiveMessage();
							
							while(!  (latestMessageFromRpi.equals("Done"))){
								System.out.println("enter while loop of message !=Done ");
								latestMessageFromRpi=client.receiveMessage();
							}
							
							
							
							arena.getExploration().calibrate();
							
							client.sendMessage(movementChar,movementNum-movementNum/2);
							latestMessageFromRpi=client.receiveMessage();
							
							while(!  (latestMessageFromRpi.equals("Done"))){
								System.out.println("enter while loop of message !=Done ");
								latestMessageFromRpi=client.receiveMessage();
							}
							
							
						}// end of else 
						
						if(arena.getNumOfTotalMovement()>2)
							arena.getExploration().calibrate();
					}//end of if real arena
					
					
						
				}// end of for loop
				
				
			}
		}).start();
		
	}
	
	public String[] encode(String source) {
	    StringBuffer dest = new StringBuffer();
	    for (int i = 0; i < source.length(); i++) {
	        int runLength = 1;
	        while (i + 1 < source.length()
	                && source.charAt(i) == source.charAt(i + 1)) {
	            runLength++;
	            i++;
	        }
	        
	        dest.append(source.charAt(i));
	        dest.append(runLength);
	        dest.append(",");
	    }
	    String[] res = dest.toString().split(",");
	    return res;
	}
	
	private int coordToNode(int x, int y) {
		return (x + y * WIDTH);
	}
	
	private int[] nodeToCoord(int pos) {
		int[] coord = new int[2];
		coord[0] = pos % WIDTH;
		coord[1] = pos / WIDTH;
		return coord;
	}
	
	private int manhattanDistance(int node1, int node2) {
		int[] coord1 = nodeToCoord(node1);
		int[] coord2 = nodeToCoord(node1);
		return Math.abs(coord1[0] - coord2[0]) + Math.abs(coord1[1] - coord1[1]);
	}
	
	private boolean isValidRobotPosition(int x, int y) {
		if (x < 1 || x > WIDTH - 2 || y < 1 || y > HEIGHT - 2) return false;
		else {
			int newY = HEIGHT - y - 1;
			
			return !(obstacles[newY][x] || obstacles[newY - 1][x - 1] || obstacles[newY - 1][x] ||
					obstacles[newY - 1][x + 1] || obstacles[newY][x + 1] || obstacles[newY + 1][x + 1] ||
					obstacles[newY + 1][x] || obstacles[newY + 1][x - 1] || obstacles[newY][x - 1]);
//			
//			return !(obstacles[newY][x] || obstacles[newY + 1][x] ||
//					obstacles[newY + 1][x - 1] || obstacles[newY][x - 1]);
		}
	}

}
