package com.mdp.simulation;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.JPanel;

public class Arena extends JPanel implements ControlsListener {
	
	
	
	private int numOfTotalMovement=0;
	
	private static final int WIDTH = 15;
	private static final int HEIGHT = 20;	
	private String robotDirection="NORTH";
	private String fakeRobotDirection;
	private boolean isRealArena=true;
	
	private int stepPerSec=25;	

	private JPanel[][] cells;
	private int robotX = 1; // 1 <= robotX <= 13
	private int robotY = 1; // 1 <= robotY <= 18	
	private BufferedImage robotImage;
	
	private BufferedImage[] robotImages = new BufferedImage[4]; // 0 -> NORTH, 1 -> EAST, 2 -> SOUTH, 3 -> WEST
	
	private Exploration exploration;
	
	private boolean[][] obstacles;// used for simulation only
	private Client client;
	
	
	
	
	
	public Exploration getExploration(){return exploration;} 
	
	
	Arena() {
		
		this.setLayout(new GridLayout(HEIGHT, WIDTH));		
		cells = new JPanel[HEIGHT][WIDTH];
		obstacles = new boolean[HEIGHT][WIDTH];
		for (int i = 0; i < HEIGHT; i++) {
			for (int j = 0; j < WIDTH; j++) {
				JPanel tempPanel = new JPanel();
				tempPanel.setBorder(BorderFactory.createLineBorder(Color.black));
				cells[i][j] = tempPanel;
				this.add(cells[i][j]);
			}
		}
		
		robotImages[0] = getRobotImage();
		robotImage = robotImages[0];
		robotImages[1] = rotateRobot(Math.toRadians(90));
		robotImages[2] = rotateRobot(Math.toRadians(180));
		robotImages[3] = rotateRobot(Math.toRadians(270));
				
		loadAndUpdateMap();
		
		if (isRealArena){
			Thread communicationThread = new Thread() {
				@Override
				public void run() {
					// TODO Auto-generated method stub
					client=new Client();
					String message=client.receiveMessage();
					
					while (message.charAt(0)!='x'|| message.charAt(2)!='y' ){//rpie send robot start position as "x2y2" , "x3y4" example
						System.out.println("enter while loop of not x3y3");
						message=client.receiveMessage();
					}
						
					int x=Character.getNumericValue(message.charAt(1));
					int y=Character.getNumericValue(message.charAt(3));
					System.out.println("x is "+x);
					System.out.println("y is "+y);
					setRobotPosition(x,y);
					
					exploration=new Exploration(Arena.this);
				}
			};// end of new thread
			
			communicationThread.start();
			
			
		}// end of if  
		else {
			exploration=new Exploration(Arena.this);
		}
	
	}
	
	
	public int getNumOfTotalMovement(){return numOfTotalMovement;}
	
	
	public void resetNumOfTotalMovement(){numOfTotalMovement=0;}
	
	
	public Client getClient(){
		return client;
	}
	
	public boolean getIsRealArena(){
		return isRealArena;
	}
	
	public void setStepPerSec(int stepPerSec){
		this.stepPerSec=stepPerSec;
	}
	
	public int getStepPerSec(){
		return stepPerSec;
	}
	
	public JPanel[][] getCells(){
		return cells;
	}
	
	
	@Override
	public void paint(Graphics g) {
		super.paint(g);
		int width = cells[0][0].getWidth(), height = cells[0][0].getHeight();
		int newRow = HEIGHT - robotY - 1, newCol = robotX - 1;
		g.drawImage(robotImage, cells[newRow][newCol].getX() + width / 2, cells[newRow][newCol].getY() - height / 2, width * 2, height * 2, null);
	}
	
	public char[][] getObstacleIndicator(){// used for simulation only
		String fileName="mdp_map.txt";
		try{
			
			FileReader fileReader = new FileReader(fileName); 			
	        BufferedReader bufferedReader = new BufferedReader(fileReader);
	        
	        char[][] obstacleIndicator=new char[HEIGHT][WIDTH];        
			for (int i=0;i<HEIGHT;i++){
				String rowString=bufferedReader.readLine();				
				for(int j=0; j<WIDTH;j++){							
					obstacleIndicator[i][j]=rowString.charAt(j);					
				}				
			}
			bufferedReader.close();
			return obstacleIndicator;
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
		return null;
		
	}
	
	
	
	void loadAndUpdateMap(){
		
			if (isRealArena==false){
				char[][] obstacleIndicator=getObstacleIndicator();
				for (int i=0;i<HEIGHT;i++){
					for(int j=0; j<WIDTH;j++){
						char c= obstacleIndicator[i][j];
						
						if (c == '1') obstacles[i][j] = true;
						else obstacles[i][j] = false;
						
						if ( c=='1') {

							cells[i][j].setBackground(Color.RED);

						}
						else if ((i>=0&&i<=2)&&(j>=12&&j<=14))
							cells[i][j].setBackground(Color.LIGHT_GRAY);
						else if ((i>=17&&i<=19)&&(j>=0&&j<=2))
							cells[i][j].setBackground(Color.GREEN);
						
						else if (i >= HEIGHT / 2)
							cells[i][j].setBackground(Color.YELLOW);
						 else 
							cells[i][j].setBackground(Color.WHITE);
										
					}// end of for width				
				}// end of for height	
			}// end of if isRealArena==false
			
			else{
				
				for (int i=0;i<HEIGHT;i++){
					for(int j=0; j<WIDTH;j++){
					   if ((i>=0&&i<=2)&&(j>=12&&j<=14))
							cells[i][j].setBackground(Color.LIGHT_GRAY);
						else if ((i>=17&&i<=19)&&(j>=0&&j<=2))
							cells[i][j].setBackground(Color.GREEN);
						
						else if (i >= HEIGHT / 2)
							cells[i][j].setBackground(Color.YELLOW);
						 else 
							cells[i][j].setBackground(Color.WHITE);
										
					}// end of for width				
				}// end of for height	
			}
		}// end of loadAndUpdateMap()
			
	

	void setRobotPosition(int xCoord, int yCoord) {
		if (xCoord > WIDTH - 1) xCoord = WIDTH - 1;
		if (yCoord > HEIGHT - 1) yCoord = HEIGHT - 1;
		robotX = xCoord;
		robotY = yCoord;
		this.repaint();	
		
	}
	
	
	

	public void setRobotDirection(String string){
		if (string.equals("NORTH")){
			robotImage = robotImages[0];
		} else if (string.equals("SOUTH")){
			robotImage = robotImages[2];
		} else if (string.equals("EAST")){
			robotImage = robotImages[1];
		} else if (string.equals("WEST")){
			robotImage = robotImages[3];
		}
		
		robotDirection=string;
	}
	
	public String getRobotDirection(){
		return robotDirection;
	}
	
	public int getRobotX(){
		return robotX;
	}
	
	public int getRobotY(){
		return robotY;
	}
	
	
	
	public void fakeTurnLeft(){
		if (getFakeRobotDirection().equals("NORTH"))
			setFakeRobotDirection("WEST");
		else if (getFakeRobotDirection().equals("SOUTH"))
			setFakeRobotDirection("EAST");
		else if (getFakeRobotDirection().equals("EAST"))
			setFakeRobotDirection("NORTH");
		else if (getFakeRobotDirection().equals("WEST"))
			setFakeRobotDirection("SOUTH");
	}
	
	public void fakeTurnRight(){

		if (getFakeRobotDirection().equals("NORTH"))
			setFakeRobotDirection("EAST");
		else if (getFakeRobotDirection().equals("SOUTH"))
			setFakeRobotDirection("WEST");
		else if (getFakeRobotDirection().equals("EAST"))
			setFakeRobotDirection("SOUTH");
		else if (getFakeRobotDirection().equals("WEST"))
			setFakeRobotDirection("NORTH");
	}
	
	public void setFakeRobotDirection(String string){
		fakeRobotDirection=string;
	}
	
	public String getFakeRobotDirection(){
		return fakeRobotDirection;
	}
	
	// move forward and backward is called only after confirmed there is 
	// no obstacle blocking
	public void moveForward(){
		
		if (getRobotDirection().equals("NORTH")){
			setRobotPosition(getRobotX(), getRobotY()+1);
			// explore area +2 
		}
			
		else if (getRobotDirection().equals("SOUTH")){
			setRobotPosition(getRobotX(), getRobotY()-1);
		}
				
		else if (getRobotDirection().equals("EAST")){
			setRobotPosition(getRobotX()+1, getRobotY());
		}
					
		else if (getRobotDirection().equals("WEST")){
			setRobotPosition(getRobotX()-1, getRobotY());
		}
		
		if (isRealArena){
			
			numOfTotalMovement++;
			if (! exploration.getIsFastModeActivated())				
					client.sendMessage('f', 1);
		
		}
	}
	
	public void moveBackward(){
		
		if (getRobotDirection().equals("NORTH")){			
			setRobotPosition(getRobotX(), getRobotY()-1);
		}
			
		else if (getRobotDirection().equals("SOUTH")){
			setRobotPosition(getRobotX(), getRobotY()+1);
			// explore area +2 
		}
				
		else if (getRobotDirection().equals("EAST")){
			setRobotPosition(getRobotX()-1, getRobotY());
		}
					
		else if (getRobotDirection().equals("WEST")){
			setRobotPosition(getRobotX()+1, getRobotY());
		}
		
		if (isRealArena){			
			numOfTotalMovement++;
			if (! exploration.getIsFastModeActivated())				
					client.sendMessage('b', 1);		
		}
		
		
	}
	// robot direction is based on perspective of robot
	public void turnLeft(){
		
		if (getRobotDirection().equals("NORTH"))
			setRobotDirection("WEST");
		else if (getRobotDirection().equals("SOUTH"))
			setRobotDirection("EAST");
		else if (getRobotDirection().equals("EAST"))
			setRobotDirection("NORTH");
		else if (getRobotDirection().equals("WEST"))
			setRobotDirection("SOUTH");
		
		if (isRealArena){			
			numOfTotalMovement++;
			if (! exploration.getIsFastModeActivated()){				
					client.sendMessage('l', 1);
			
			}
		}

		repaint();
	}
	
	
	
	public void turnRight(){
		
		if (getRobotDirection().equals("NORTH"))
			setRobotDirection("EAST");
		else if (getRobotDirection().equals("SOUTH"))
			setRobotDirection("WEST");
		else if (getRobotDirection().equals("EAST"))
			setRobotDirection("SOUTH");
		else if (getRobotDirection().equals("WEST"))
			setRobotDirection("NORTH");
		
		rotateRobot(Math.toRadians(90));
		
		if (isRealArena){			
			numOfTotalMovement++;
			if (! exploration.getIsFastModeActivated())				
					client.sendMessage('r', 1);		
			
		}
		
		
		repaint();
		
		
	}
	

	@Override
	public void findShortestPath() {
		
		
		FastestPath fastestPath = new FastestPath(this, exploration.transformObstacleMapWithUnknown());
		fastestPath.moveRobot(fastestPath.fastestPath(1, 1, 13, 18));
	}

 
	@Override
	public void resetMap() {
		loadAndUpdateMap();
	}
	
	private BufferedImage getRobotImage() {
		BufferedImage image = null;
		try {
			image = ImageIO.read(new File("robot.png"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return image;
	}
	
	private BufferedImage rotateRobot(double angle) {
		AffineTransform at = new AffineTransform();
		at.rotate(angle, robotImage.getWidth() / 2, robotImage.getHeight() / 2);
		AffineTransformOp atOp = new AffineTransformOp(at, AffineTransformOp.TYPE_BILINEAR);
		return atOp.filter(robotImage, null);
	}

	@Override
	public void explore() {
		exploration.explore();
	}

	@Override
	public void setPercentageExplored(int percentage) {
		exploration.setPercentageExplored(percentage);
	}

	@Override
	public void setTimeLimit(int timeLimit) {
		exploration.setTimeLimit(timeLimit);
		exploration.setExploredByTimeLimit();
	}
}
