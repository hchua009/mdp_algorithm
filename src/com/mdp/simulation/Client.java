package com.mdp.simulation;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;


public class Client  {
	private String hostName;
	private int portNumber;
	private Socket socket;
	private PrintWriter messageToRpie;
	private BufferedReader messageFromRpie;
	
	public Client() {		
		hostName = "192.168.50.50";
		portNumber = 5182;
		try {
			socket = new Socket(hostName, portNumber);
			messageToRpie = new PrintWriter(socket.getOutputStream(), true);
			messageFromRpie =new BufferedReader(new InputStreamReader(socket.getInputStream()));
			socket.setSoTimeout(6000);
		} catch (Exception e) {			
			e.printStackTrace();
		} 
	}
	
	
	public void sendFullString(String string){
		messageToRpie.println(string);
		messageToRpie.flush();
		System.out.println("algorithm sends rpie map descriptor in hex: "+string);
	}
	
	// uppercase direction - only movement
	// lowercase direction - move + sense 
	public void sendMessage(char direction , int magnitude){
		// f, b,l,r
		String messageToSend=null;
		if (magnitude <10)
			messageToSend=""+direction+"0"+magnitude;	// eg f5, l2 means turn left by 90degree 2 time
		else 
			messageToSend=""+direction+""+magnitude;
		
		
		
		// send to rpie	    	
		messageToRpie.println(messageToSend);
		messageToRpie.flush();
		System.out.println("algorithm sends arduino: "+messageToSend);
	}
	
	private static final long TIMEOUT = 9000;
	
	
	public String receiveMessage() {
		// receive from 5 different sensor 
		// receive number which is saying the obstacle is how many block away
		// the number can be 0,1,2 
		String receiveMessage=null;
		long timeInit = System.currentTimeMillis();
		
		// receive from rpie
		try{			
			
			receiveMessage=messageFromRpie.readLine();
			while (receiveMessage==null || receiveMessage.equals("")){
				System.out.println("inside while loop of receiveMessage==null");
				
				receiveMessage=messageFromRpie.readLine();
				System.out.println("reach line after messageFromRpie.readLine()");
				/*if (System.currentTimeMillis() - timeInit >= TIMEOUT) {
					receiveMessage = "TIMEOUT";
					break;
				}*/
			}
			System.out.println("Client receive: " + receiveMessage);
		}
		catch (Exception e){
			System.out.println("enter exception handling in receiveMessage()");			
			//e.printStackTrace();
			return "TIMEOUT";
		}
		 
		return receiveMessage;
		
				
	}
	
	
	
}
