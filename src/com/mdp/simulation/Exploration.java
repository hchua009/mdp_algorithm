package com.mdp.simulation;

import java.awt.Color;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.PriorityQueue;

public class Exploration {
	Arena arena;

	// c0 mean calibrate left client.sendMessage('c', 0);
	// c1 mean calibrate right client.sendMessage('c', 1);

	private ControlsListener controlsListener;
	private boolean fastModeActivated=false;
	private static final int WIDTH = 15;
	private static final int HEIGHT = 20;
	private int noOfGridExplored=0;
	private double currentCoveragePercentage=0;
	private int targetCoveragePercentage=91;
	private int timeLimit=270;
	// i set timeLimit to 4 min 30 sec so that there is enough time of 1min 30secs left for robot go back start zone
	// this is because if by 6min robot still not in start zone, we are disqualitied for leaderboard b
	private boolean exploreByPercentageExplored=false;
	private boolean exploreByTimeLimit=false;
	private boolean[][] isExplored=new boolean[HEIGHT][WIDTH];;
	// 0 for not obstacle 1 for obstacle 2 for unknown

	private int[][] isObstacle=new int[HEIGHT][WIDTH];// actual obstacle from text file
	private boolean[][] walkBefore=new boolean[HEIGHT][WIDTH];

	// 0 for not obstacle 1 for obstacle 2 for unknown
	private int[][] obstacleSensedByRobot=new int[HEIGHT][WIDTH];
	private int[][] obstacleSensedBySensor = new int[HEIGHT][WIDTH];

	private Client client;
	private String latestMessageFromRpi="";

	public boolean getIsFastModeActivated(){return fastModeActivated;}


	public Exploration(Arena arena) {

		arena.setRobotPosition(arena.getRobotX(), arena.getRobotY());

		this.arena = arena;
		client=arena.getClient();
		if (arena.getIsRealArena()==false){
			char[][] obstacleIndicator=arena.getObstacleIndicator();
			for (int i = 0; i < HEIGHT; i++) {
				for (int j = 0; j < WIDTH; j++) {
					isExplored[i][j]=false;
					if ( obstacleIndicator[i][j]=='1')
						isObstacle[i][j]=1;
					else
						isObstacle[i][j]=0;

					obstacleSensedByRobot[i][j]=2;// initialise ALL obstacle to be unknown

				}
			}

		}// end of if fake arena
		else{


			for (int i = 0; i < HEIGHT; i++) {
				for (int j = 0; j < WIDTH; j++) {
					isExplored[i][j]=false;
					obstacleSensedByRobot[i][j]=2;// initialise ALL obstacle to be unknown
					walkBefore[i][j]=false;
				}// end of for j
			}// end of for i




			explore();

		}//end of else




	}

	public void setStartZoneAsExploredAndNoObstacle(){
		// set start zone as explored
		obstacleSensedByRobot[19][0] = 0;
		obstacleSensedByRobot[19][1] = 0;
		obstacleSensedByRobot[19][2] = 0;
		obstacleSensedByRobot[18][0] = 0;
		obstacleSensedByRobot[18][1] = 0;
		obstacleSensedByRobot[18][2] = 0;
		obstacleSensedByRobot[17][0] = 0;
		obstacleSensedByRobot[17][1] = 0;
		obstacleSensedByRobot[17][2] = 0;

		setIsExplored(19, 0);
		setIsExplored(19, 1);
		setIsExplored(19, 2);
		setIsExplored(18, 0);
		setIsExplored(18, 1);
		setIsExplored(18, 2);
		setIsExplored(17, 0);
		setIsExplored(17, 1);
		setIsExplored(17, 2);
	}


	public int getNumOfGridExplored(){
		return noOfGridExplored;
	}

	public double getCurrentCoveragePercentage(){
		return currentCoveragePercentage;
	}

	public void setExploredByTimeLimit(){
		exploreByTimeLimit=true;
	}

	public int[][] getIsObstacle(){// from text file
		return isObstacle;
	}

	public boolean[][] getIsExplored(){
		return isExplored;
	}


	// this method will do the setting of obstacle also
	// this method is called by sense() method
	public void setIsExplored(int rowNumber,int columnNumber){
		if (rowNumber<0|| rowNumber>19 || columnNumber <0 || columnNumber>14  )
			return;


		if (!getIsExplored()[rowNumber][columnNumber]){// grid has not been explored
			isExplored[rowNumber][columnNumber]=true;
			noOfGridExplored++;
			currentCoveragePercentage=((double)noOfGridExplored/300)*100;

			if (arena.getIsRealArena()==false){
				// after set as explored, sense if it is obstacle
				if(getIsObstacle()[rowNumber][columnNumber]==1)
					obstacleSensedByRobot[rowNumber][columnNumber]=1;
				else
					obstacleSensedByRobot[rowNumber][columnNumber]=0;
			}

			else{
				// here nothing because obstacleSensedByRobot[][] will be updated
				// by sense() method if arena is real life leaderboard
			}

		}	//end of if not explored
		if (obstacleSensedByRobot[rowNumber][columnNumber]==0)// not obstacle
			arena.getCells()[rowNumber][columnNumber].setBackground(Color.PINK);
		else if (obstacleSensedByRobot[rowNumber][columnNumber]==1)
			arena.getCells()[rowNumber][columnNumber].setBackground(Color.BLACK);
	}

	public void setObstacleSensedByRobot(int rowNumber,int columnNumber, int truth, int fromSensor){
		if (rowNumber<0|| rowNumber>19 || columnNumber <0 || columnNumber>14  )
			return;


		// start and goal zone and perimeter of start and goal zone cannot be obstacle
		 if (
			(rowNumber==17&&columnNumber==0) || (rowNumber==17&&columnNumber==1) ||		// start zone
		 	(rowNumber==17&&columnNumber==2) || (rowNumber==18&&columnNumber==0) ||
		 	(rowNumber==18&&columnNumber==1) || (rowNumber==18&&columnNumber==2) ||
		 	(rowNumber==19&&columnNumber==0) || (rowNumber==19&&columnNumber==1) ||
		 	(rowNumber==19&&columnNumber==2) ||


 			(rowNumber==0&&columnNumber==12) || (rowNumber==1&&columnNumber==12) ||		 	// goal zone
		 	(rowNumber==2&&columnNumber==12) || (rowNumber==0&&columnNumber==13) ||
		 	(rowNumber==1&&columnNumber==13) || (rowNumber==2&&columnNumber==13) ||
		 	(rowNumber==0&&columnNumber==14) || (rowNumber==1&&columnNumber==14) ||
		 	(rowNumber==2&&columnNumber==14)


			){
			obstacleSensedByRobot[rowNumber][columnNumber]=0;
		 	return;
		 }

		 if(walkBefore[rowNumber][ columnNumber]==true){
			obstacleSensedByRobot[rowNumber][columnNumber]=0;
		 	return;
		 }
			 
		if (obstacleSensedByRobot[rowNumber][columnNumber]==2 || 
				obstacleSensedBySensor[rowNumber][columnNumber] != 0) {// unknown obstacle then update
			obstacleSensedByRobot[rowNumber][columnNumber]=truth;
			obstacleSensedBySensor[rowNumber][columnNumber] = fromSensor;
		}


		// else obstacle already known so dont keep updating. Believe the very first sensor reading only


	}

	public void setTimeLimit(int timeLimit){
		this.timeLimit=timeLimit;
		exploreByTimeLimit=true;
	}

	public void setIsObstacle(int rowNumber,int columnNumber, int value){// for fake arena
		getIsObstacle()[rowNumber][columnNumber]=value;
	}

	public void setPercentageExplored(int percentage){
		targetCoveragePercentage=percentage;
		exploreByPercentageExplored=true;
	}

	// move forward is called only after confirmed there is
		// no obstacle blocking
	public void moveForward(){

		arena.moveForward();// the 3 new grid that robot step into is considered as explored
		if (arena.getRobotDirection().equals("NORTH")){
			// explore area +3
			setIsExplored(18-arena.getRobotY(),arena.getRobotX()-1);
			setIsExplored(18-arena.getRobotY(),arena.getRobotX());
			setIsExplored(18-arena.getRobotY(),arena.getRobotX()+1);
		}

		else if (arena.getRobotDirection().equals("SOUTH")){
			setIsExplored(20-arena.getRobotY(),arena.getRobotX()-1);
			setIsExplored(20-arena.getRobotY(),arena.getRobotX());
			setIsExplored(20-arena.getRobotY(),arena.getRobotX()+1);
		}

		else if (arena.getRobotDirection().equals("EAST")){
			setIsExplored(18-arena.getRobotY(),arena.getRobotX()+1);
			setIsExplored(19-arena.getRobotY(),arena.getRobotX()+1);
			setIsExplored(20-arena.getRobotY(),arena.getRobotX()+1);
		}

		else if (arena.getRobotDirection().equals("WEST")){
			setIsExplored(18-arena.getRobotY(),arena.getRobotX()-1);
			setIsExplored(19-arena.getRobotY(),arena.getRobotX()-1);
			setIsExplored(20-arena.getRobotY(),arena.getRobotX()-1);
		}
	}

	// this method will call setIsExplored() after sensing is done
	public void northSense(){// sense method when robot direction is north
		//System.out.println("inside north sense method ");
		if (arena.getIsRealArena()==false){
			if (arena.getRobotY()!=18){// not near top wall

				// sense the 3 grid in front of it
				setIsExplored( 17-arena.getRobotY(), arena.getRobotX()-1);
				setIsExplored( 17-arena.getRobotY(), arena.getRobotX());
				setIsExplored( 17-arena.getRobotY(), arena.getRobotX()+1);

				// further sensing of top left grid if possible
				if (isObstacle[17-arena.getRobotY()][arena.getRobotX()-1]!=1&& (arena.getRobotY()+1!=18)) {// not obstacle
					setIsExplored( 16-arena.getRobotY(),arena.getRobotX()-1 );

				}

				// further sensing of top middle grid if possible
				if (isObstacle[17-arena.getRobotY()][arena.getRobotX()]!=1&& (arena.getRobotY()+1!=18)){// not obstacle
					setIsExplored(16-arena.getRobotY() ,arena.getRobotX() );

				}
				// further sensing of top right grid if possible
				if (isObstacle[17-arena.getRobotY()][arena.getRobotX()+1]!=1&& (arena.getRobotY()+1!=18)){// not obstacle
					setIsExplored(16-arena.getRobotY() , arena.getRobotX()+1);

				}
			}

			if (arena.getRobotX()!=1){// not near left wall
				// sense left grid and if possible further sensing
				setIsExplored( 18-arena.getRobotY(), arena.getRobotX()-2);
				if (isObstacle[18-arena.getRobotY()][arena.getRobotX()-2]!=1 && (arena.getRobotX()-1 !=1))// not obstacle
					setIsExplored(18-arena.getRobotY() , arena.getRobotX()-3);
			}



			if (arena.getRobotX()!=13){// not near right wall
				// sense right grid and if possible further sensing
				setIsExplored( 18-arena.getRobotY(), arena.getRobotX()+2);
				if (isObstacle[18-arena.getRobotY()][arena.getRobotX()+2]!=1 && (arena.getRobotX()-1 !=1) )// not obstacle
					setIsExplored(18-arena.getRobotY() , arena.getRobotX()+3);
			}
		}// end of if fake arena

		else{


			if (latestMessageFromRpi.charAt(1)=='0'){
				setObstacleSensedByRobot(18-arena.getRobotY(),arena.getRobotX()-2,1, 0);
				setIsExplored(18-arena.getRobotY(),arena.getRobotX()-2);

			}
			else if (latestMessageFromRpi.charAt(1)=='1'){
				setObstacleSensedByRobot(18-arena.getRobotY(),arena.getRobotX()-2,0, 1);
				setIsExplored(18-arena.getRobotY(),arena.getRobotX()-2);
				setObstacleSensedByRobot(18-arena.getRobotY(),arena.getRobotX()-3,1, 1);
				setIsExplored(18-arena.getRobotY(),arena.getRobotX()-3);


			}
			else{
				setObstacleSensedByRobot(18-arena.getRobotY(),arena.getRobotX()-2,0, 9);
				setIsExplored(18-arena.getRobotY(),arena.getRobotX()-2);
				setObstacleSensedByRobot(18-arena.getRobotY(),arena.getRobotX()-3,0, 9);
				setIsExplored(18-arena.getRobotY(),arena.getRobotX()-3);

			}



			if (latestMessageFromRpi.charAt(2)=='0'){
				setObstacleSensedByRobot(17-arena.getRobotY(),arena.getRobotX()-1,1, 0);
				setIsExplored(17-arena.getRobotY(),arena.getRobotX()-1);

			}
			else if (latestMessageFromRpi.charAt(2)=='1'){
				setObstacleSensedByRobot(17-arena.getRobotY(),arena.getRobotX()-1,0, 1);
				setIsExplored(17-arena.getRobotY(),arena.getRobotX()-1);
				setObstacleSensedByRobot(16-arena.getRobotY(),arena.getRobotX()-1,1, 1);
				setIsExplored(16-arena.getRobotY(),arena.getRobotX()-1);

			}


			else{
				setObstacleSensedByRobot(17-arena.getRobotY(),arena.getRobotX()-1,0, 9);
				setIsExplored(17-arena.getRobotY(),arena.getRobotX()-1);
				setObstacleSensedByRobot(16-arena.getRobotY(),arena.getRobotX()-1,0, 9);
				setIsExplored(16-arena.getRobotY(),arena.getRobotX()-1);

			}



			if (latestMessageFromRpi.charAt(3)=='0'){
				setObstacleSensedByRobot(17-arena.getRobotY(),arena.getRobotX(),1, 0);
				setIsExplored(17-arena.getRobotY(),arena.getRobotX());
			}
			else if (latestMessageFromRpi.charAt(3)=='1'){
				setObstacleSensedByRobot(17-arena.getRobotY(),arena.getRobotX(),0, 1);
				setIsExplored(17-arena.getRobotY(),arena.getRobotX());

				setObstacleSensedByRobot(16-arena.getRobotY(),arena.getRobotX(),1, 1);
				setIsExplored(16-arena.getRobotY(),arena.getRobotX());

			}


			else{
				setObstacleSensedByRobot(17-arena.getRobotY(),arena.getRobotX(),0, 9);
				setIsExplored(17-arena.getRobotY(),arena.getRobotX());

				setObstacleSensedByRobot(16-arena.getRobotY(),arena.getRobotX(),0, 9);
				setIsExplored(16-arena.getRobotY(),arena.getRobotX());

			}


			if (latestMessageFromRpi.charAt(4)=='0'){
				setObstacleSensedByRobot(17-arena.getRobotY(),arena.getRobotX()+1,1, 0);
				setIsExplored(17-arena.getRobotY(),arena.getRobotX()+1);

			}
			else if (latestMessageFromRpi.charAt(4)=='1'){
				setObstacleSensedByRobot(17-arena.getRobotY(),arena.getRobotX()+1,0, 1);
				setIsExplored(17-arena.getRobotY(),arena.getRobotX()+1);
				setObstacleSensedByRobot(16-arena.getRobotY(),arena.getRobotX()+1,1, 1);
				setIsExplored(16-arena.getRobotY(),arena.getRobotX()+1);


			}


			else{
				setObstacleSensedByRobot(17-arena.getRobotY(),arena.getRobotX()+1,0, 9);
				setIsExplored(17-arena.getRobotY(),arena.getRobotX()+1);
				setObstacleSensedByRobot(16-arena.getRobotY(),arena.getRobotX()+1,0, 9);
				setIsExplored(16-arena.getRobotY(),arena.getRobotX()+1);

			}





			if (latestMessageFromRpi.charAt(5)=='0'){
				setObstacleSensedByRobot(18-arena.getRobotY(),arena.getRobotX()+2,1, 0);
				setIsExplored(18-arena.getRobotY(),arena.getRobotX()+2);

			}
			else if (latestMessageFromRpi.charAt(5)=='1'){
				setObstacleSensedByRobot(18-arena.getRobotY(),arena.getRobotX()+2,0, 1);
				setIsExplored(18-arena.getRobotY(),arena.getRobotX()+2);
				setObstacleSensedByRobot(18-arena.getRobotY(),arena.getRobotX()+3,1, 1);
				setIsExplored(18-arena.getRobotY(),arena.getRobotX()+3);


			}
			else{
				setObstacleSensedByRobot(18-arena.getRobotY(),arena.getRobotX()+2,0, 9);
				setIsExplored(18-arena.getRobotY(),arena.getRobotX()+2);
				setObstacleSensedByRobot(18-arena.getRobotY(),arena.getRobotX()+3,0, 9);
				setIsExplored(18-arena.getRobotY(),arena.getRobotX()+3);

			}


		}// end of else
	}// end of northSense()



	// this method will call setIsExplored() after sensing is done
	public void southSense(){//sense method when robot direction is south
		//System.out.println("inside south sense method ");
		if (arena.getIsRealArena()==false){
			if (arena.getRobotY()!=1){// not near top wall

				// sense the 3 grid in front of it
				setIsExplored( 21-arena.getRobotY(), arena.getRobotX()-1);
				setIsExplored( 21-arena.getRobotY(), arena.getRobotX());
				setIsExplored( 21-arena.getRobotY(), arena.getRobotX()+1);


				// further sensing of bottom left grid if possible
				if (isObstacle[21-arena.getRobotY()][arena.getRobotX()-1]!=1&& (arena.getRobotY()-1!=1)) {// not obstacle
					setIsExplored( 22-arena.getRobotY(),arena.getRobotX()-1 );

				}


				// further sensing of bottom middle grid if possible
				if (isObstacle[21-arena.getRobotY()][arena.getRobotX()]!=1&& (arena.getRobotY()-1!=1)){// not obstacle
					setIsExplored(22-arena.getRobotY() ,arena.getRobotX() );

				}

				// further sensing of bottom right grid if possible
				if (isObstacle[21-arena.getRobotY()][arena.getRobotX()+1]!=1&& (arena.getRobotY()-1!=1)){// not obstacle
					setIsExplored(22-arena.getRobotY() , arena.getRobotX()+1);

				}
			}




			if (arena.getRobotX()!=1){// not near left wall
				// sense left grid and if possible further sensing
				setIsExplored( 20-arena.getRobotY(), arena.getRobotX()-2);
				if (isObstacle[20-arena.getRobotY()][arena.getRobotX()-2]!=1 && (arena.getRobotX()-1 !=1))// not obstacle
					setIsExplored(20-arena.getRobotY() , arena.getRobotX()-3);
			}



			if (arena.getRobotX()!=13){// not near right wall
				// sense right grid and if possible further sensing
				setIsExplored( 20-arena.getRobotY(), arena.getRobotX()+2);
				if (isObstacle[20-arena.getRobotY()][arena.getRobotX()+2]!=1 && (arena.getRobotX()+1!=13) )// not obstacle
					setIsExplored(20-arena.getRobotY() , arena.getRobotX()+3);
			}
		}// end of arena.getIsRealArena()==false

		else{


			if (latestMessageFromRpi.charAt(1)=='0'){
				setObstacleSensedByRobot(20-arena.getRobotY(),arena.getRobotX()+2,1, 0);
				setIsExplored(20-arena.getRobotY(),arena.getRobotX()+2);

			}
			else if (latestMessageFromRpi.charAt(1)=='1'){
				setObstacleSensedByRobot(20-arena.getRobotY(),arena.getRobotX()+2,0, 1);
				setIsExplored(20-arena.getRobotY(),arena.getRobotX()+2);

				setObstacleSensedByRobot(20-arena.getRobotY(),arena.getRobotX()+3,1, 1);
				setIsExplored(20-arena.getRobotY(),arena.getRobotX()+3);


			}
			else{
				setObstacleSensedByRobot(20-arena.getRobotY(),arena.getRobotX()+2,0, 9);
				setIsExplored(20-arena.getRobotY(),arena.getRobotX()+2);
				setObstacleSensedByRobot(20-arena.getRobotY(),arena.getRobotX()+3,0, 9);
				setIsExplored(20-arena.getRobotY(),arena.getRobotX()+3);

			}



			if (latestMessageFromRpi.charAt(2)=='0'){
				setObstacleSensedByRobot(21-arena.getRobotY(),arena.getRobotX()+1,1, 0);
				setIsExplored(21-arena.getRobotY(),arena.getRobotX()+1);

			}
			else if (latestMessageFromRpi.charAt(2)=='1'){
				setObstacleSensedByRobot(21-arena.getRobotY(),arena.getRobotX()+1,0, 1);
				setIsExplored(21-arena.getRobotY(),arena.getRobotX()+1);
				setObstacleSensedByRobot(22-arena.getRobotY(),arena.getRobotX()+1,1, 1);
				setIsExplored(22-arena.getRobotY(),arena.getRobotX()+1);


			}


			else{
				setObstacleSensedByRobot(21-arena.getRobotY(),arena.getRobotX()+1,0, 9);
				setIsExplored(21-arena.getRobotY(),arena.getRobotX()+1);
				setObstacleSensedByRobot(22-arena.getRobotY(),arena.getRobotX()+1,0, 9);
				setIsExplored(22-arena.getRobotY(),arena.getRobotX()+1);

			}



			if (latestMessageFromRpi.charAt(3)=='0'){
				setObstacleSensedByRobot(21-arena.getRobotY(),arena.getRobotX(),1, 0);
				setIsExplored(21-arena.getRobotY(),arena.getRobotX());

			}
			else if (latestMessageFromRpi.charAt(3)=='1'){
				setObstacleSensedByRobot(21-arena.getRobotY(),arena.getRobotX(),0, 1);
				setIsExplored(21-arena.getRobotY(),arena.getRobotX());
				setObstacleSensedByRobot(22-arena.getRobotY(),arena.getRobotX(),1, 1);
				setIsExplored(22-arena.getRobotY(),arena.getRobotX());


			}


			else{
				setObstacleSensedByRobot(21-arena.getRobotY(),arena.getRobotX(),0, 9);
				setIsExplored(21-arena.getRobotY(),arena.getRobotX());
				setObstacleSensedByRobot(22-arena.getRobotY(),arena.getRobotX(),0, 9);
				setIsExplored(22-arena.getRobotY(),arena.getRobotX());

			}


			if (latestMessageFromRpi.charAt(4)=='0'){
				setObstacleSensedByRobot(21-arena.getRobotY(),arena.getRobotX()-1,1, 0);
				setIsExplored(21-arena.getRobotY(),arena.getRobotX()-1);

			}
			else if (latestMessageFromRpi.charAt(4)=='1'){
				setObstacleSensedByRobot(21-arena.getRobotY(),arena.getRobotX()-1,0, 1);
				setIsExplored(21-arena.getRobotY(),arena.getRobotX()-1);
				setObstacleSensedByRobot(22-arena.getRobotY(),arena.getRobotX()-1,1, 1);
				setIsExplored(22-arena.getRobotY(),arena.getRobotX()-1);


			}

			else{
				setObstacleSensedByRobot(21-arena.getRobotY(),arena.getRobotX()-1,0, 9);
				setIsExplored(21-arena.getRobotY(),arena.getRobotX()-1);
				setObstacleSensedByRobot(22-arena.getRobotY(),arena.getRobotX()-1,0, 9);
				setIsExplored(22-arena.getRobotY(),arena.getRobotX()-1);

			}





			if (latestMessageFromRpi.charAt(5)=='0'){
				setObstacleSensedByRobot(20-arena.getRobotY(),arena.getRobotX()-2,1, 0);
				setIsExplored(20-arena.getRobotY(),arena.getRobotX()-2);

			}
			else if (latestMessageFromRpi.charAt(5)=='1'){
				setObstacleSensedByRobot(20-arena.getRobotY(),arena.getRobotX()-2,0, 1);
				setIsExplored(20-arena.getRobotY(),arena.getRobotX()-2);
				setObstacleSensedByRobot(20-arena.getRobotY(),arena.getRobotX()-3,1, 1);
				setIsExplored(20-arena.getRobotY(),arena.getRobotX()-3);


			}
			else{
				setObstacleSensedByRobot(20-arena.getRobotY(),arena.getRobotX()-2,0, 9);
				setIsExplored(20-arena.getRobotY(),arena.getRobotX()-2);
				setObstacleSensedByRobot(20-arena.getRobotY(),arena.getRobotX()-3,0, 9);
				setIsExplored(20-arena.getRobotY(),arena.getRobotX()-3);

			}


		}// end of else

	}





	// this method will call setIsExplored() after sensing is done
	public void eastSense(){//sense method when robot direction is east
		//System.out.println("inside east sense method ");
		if (arena.getIsRealArena()==false){
			if (arena.getRobotX()!=13){// not near top wall

				// sense the 3 grid in front of it
				setIsExplored( 18-arena.getRobotY(), arena.getRobotX()+2);
				setIsExplored( 19-arena.getRobotY(), arena.getRobotX()+2);
				setIsExplored( 20-arena.getRobotY(), arena.getRobotX()+2);


				// further sensing of right top grid if possible
				if (isObstacle[18-arena.getRobotY()][arena.getRobotX()+2]!=1&& (arena.getRobotX()+1!=13)) {// not obstacle
					setIsExplored( 18-arena.getRobotY(),arena.getRobotX()+3 );

				}


				// further sensing of right middle grid if possible
				if (isObstacle[19-arena.getRobotY()][arena.getRobotX()+2]!=1&& (arena.getRobotX()+1!=13)){// not obstacle
					setIsExplored(19-arena.getRobotY() ,arena.getRobotX()+3 );

				}

				// further sensing of right bottom grid if possible
				if (isObstacle[20-arena.getRobotY()][arena.getRobotX()+2]!=1&& (arena.getRobotX()+1!=13)){// not obstacle
					setIsExplored(20-arena.getRobotY() , arena.getRobotX()+3);

				}
			}




			if (arena.getRobotY()!=18){// not near top wall
				// sense top grid and if possible further sensing
				setIsExplored( 17-arena.getRobotY(), arena.getRobotX()+1);
				if (isObstacle[17-arena.getRobotY()][arena.getRobotX()+1]!=1 && (arena.getRobotY()+1!=18))// not obstacle
					setIsExplored(16-arena.getRobotY() , arena.getRobotX()+1);
			}



			if (arena.getRobotY()!=1){// not near right wall
				// sense bottom grid and if possible further sensing
				setIsExplored( 21-arena.getRobotY(),arena.getRobotX()+1);
				if (isObstacle[21-arena.getRobotY()][arena.getRobotX()+1]!=1 && (arena.getRobotY()-1!=1) )// not obstacle
					setIsExplored(22-arena.getRobotY() , arena.getRobotX()+1);
			}
		}// end of if
		else{


			if (latestMessageFromRpi.charAt(1)=='0'){
				setObstacleSensedByRobot(17-arena.getRobotY(),arena.getRobotX()+1,1, 0);
				setIsExplored(17-arena.getRobotY(),arena.getRobotX()+1);

			}
			else if (latestMessageFromRpi.charAt(1)=='1'){
				setObstacleSensedByRobot(17-arena.getRobotY(),arena.getRobotX()+1,0, 1);
				setIsExplored(17-arena.getRobotY(),arena.getRobotX()+1);
				setObstacleSensedByRobot(16-arena.getRobotY(),arena.getRobotX()+1,1, 1);
				setIsExplored(16-arena.getRobotY(),arena.getRobotX()+1);


			}
			else{
				setObstacleSensedByRobot(17-arena.getRobotY(),arena.getRobotX()+1,0, 9);
				setIsExplored(17-arena.getRobotY(),arena.getRobotX()+1);
				setObstacleSensedByRobot(16-arena.getRobotY(),arena.getRobotX()+1,0, 9);
				setIsExplored(16-arena.getRobotY(),arena.getRobotX()+1);

			}



			if (latestMessageFromRpi.charAt(2)=='0'){
				setObstacleSensedByRobot(18-arena.getRobotY(),arena.getRobotX()+2,1, 0);
				setIsExplored(18-arena.getRobotY(),arena.getRobotX()+2);

			}
			else if (latestMessageFromRpi.charAt(2)=='1'){
				setObstacleSensedByRobot(18-arena.getRobotY(),arena.getRobotX()+2,0, 1);
				setIsExplored(18-arena.getRobotY(),arena.getRobotX()+2);
				setObstacleSensedByRobot(18-arena.getRobotY(),arena.getRobotX()+3,1, 1);
				setIsExplored(18-arena.getRobotY(),arena.getRobotX()+3);


			}

			else{
				setObstacleSensedByRobot(18-arena.getRobotY(),arena.getRobotX()+2,0, 9);
				setIsExplored(18-arena.getRobotY(),arena.getRobotX()+2);
				setObstacleSensedByRobot(18-arena.getRobotY(),arena.getRobotX()+3,0, 9);
				setIsExplored(18-arena.getRobotY(),arena.getRobotX()+3);

			}



			if (latestMessageFromRpi.charAt(3)=='0'){
				setObstacleSensedByRobot(19-arena.getRobotY(),arena.getRobotX()+2,1, 0);
				setIsExplored(19-arena.getRobotY(),arena.getRobotX()+2);

			}
			else if (latestMessageFromRpi.charAt(3)=='1'){
				setObstacleSensedByRobot(19-arena.getRobotY(),arena.getRobotX()+2,0, 1);
				setIsExplored(19-arena.getRobotY(),arena.getRobotX()+2);
				setObstacleSensedByRobot(19-arena.getRobotY(),arena.getRobotX()+3,1, 1);
				setIsExplored(19-arena.getRobotY(),arena.getRobotX()+3);


			}

			else{
				setObstacleSensedByRobot(19-arena.getRobotY(),arena.getRobotX()+2,0, 9);
				setIsExplored(19-arena.getRobotY(),arena.getRobotX()+2);
				setObstacleSensedByRobot(19-arena.getRobotY(),arena.getRobotX()+3,0, 9);
				setIsExplored(19-arena.getRobotY(),arena.getRobotX()+3);

			}


			if (latestMessageFromRpi.charAt(4)=='0'){
				setObstacleSensedByRobot(20-arena.getRobotY(),arena.getRobotX()+2,1, 0);
				setIsExplored(20-arena.getRobotY(),arena.getRobotX()+2);

			}
			else if (latestMessageFromRpi.charAt(4)=='1'){
				setObstacleSensedByRobot(20-arena.getRobotY(),arena.getRobotX()+2,0, 1);
				setIsExplored(20-arena.getRobotY(),arena.getRobotX()+2);
				setObstacleSensedByRobot(20-arena.getRobotY(),arena.getRobotX()+3,1, 1);
				setIsExplored(20-arena.getRobotY(),arena.getRobotX()+3);


			}

			else{
				setObstacleSensedByRobot(20-arena.getRobotY(),arena.getRobotX()+2,0, 9);
				setIsExplored(20-arena.getRobotY(),arena.getRobotX()+2);
				setObstacleSensedByRobot(20-arena.getRobotY(),arena.getRobotX()+3,0, 9);
				setIsExplored(20-arena.getRobotY(),arena.getRobotX()+3);

			}





			if (latestMessageFromRpi.charAt(5)=='0'){
				setObstacleSensedByRobot(21-arena.getRobotY(),arena.getRobotX()+1,1, 0);
				setIsExplored(21-arena.getRobotY(),arena.getRobotX()+1);

			}
			else if (latestMessageFromRpi.charAt(5)=='1'){
				setObstacleSensedByRobot(21-arena.getRobotY(),arena.getRobotX()+1,0, 1);
				setIsExplored(21-arena.getRobotY(),arena.getRobotX()+1);
				setObstacleSensedByRobot(22-arena.getRobotY(),arena.getRobotX()+1,1, 1);
				setIsExplored(22-arena.getRobotY(),arena.getRobotX()+1);


			}
			else{
				setObstacleSensedByRobot(21-arena.getRobotY(),arena.getRobotX()+1,0, 9);
				setIsExplored(21-arena.getRobotY(),arena.getRobotX()+1);
				setObstacleSensedByRobot(22-arena.getRobotY(),arena.getRobotX()+1,0, 9);
				setIsExplored(22-arena.getRobotY(),arena.getRobotX()+1);

			}


		}// end of else
	}




	// this method will call setIsExplored() after sensing is done
	public void westSense(){//sense method when robot direction is west
		//System.out.println("inside west sense method ");
		if (arena.getIsRealArena()==false){
			if (arena.getRobotX()!=1){// not near bottom wall

				// sense the 3 grid in front of it
				setIsExplored( 18-arena.getRobotY(), arena.getRobotX()-2);
				setIsExplored( 19-arena.getRobotY(), arena.getRobotX()-2);
				setIsExplored( 20-arena.getRobotY(), arena.getRobotX()-2);


				// further sensing of left top grid if possible
				if (isObstacle[18-arena.getRobotY()][arena.getRobotX()-2]!=1&& (arena.getRobotX()-1!=1)) {// not obstacle
					setIsExplored( 18-arena.getRobotY(),arena.getRobotX()-3 );

				}


				// further sensing of left middle grid if possible
				if (isObstacle[19-arena.getRobotY()][arena.getRobotX()-2]!=1&& (arena.getRobotX()-1!=1)){// not obstacle
					setIsExplored(19-arena.getRobotY() ,arena.getRobotX()-3 );

				}

				// further sensing of right right grid if possible
				if (isObstacle[20-arena.getRobotY()][arena.getRobotX()-2]!=1&& (arena.getRobotX()-1!=1)){// not obstacle
					setIsExplored(20-arena.getRobotY() , arena.getRobotX()-3);

				}
			}




			if (arena.getRobotY()!=18){// not near top wall
				// sense top grid and if possible further sensing
				setIsExplored( 17-arena.getRobotY(), arena.getRobotX()-1);
				if (isObstacle[17-arena.getRobotY()][arena.getRobotX()-1]!=1 && (arena.getRobotY()+1!=18))// not obstacle
					setIsExplored(16-arena.getRobotY() , arena.getRobotX()-1);
			}



			if (arena.getRobotY()!=1){// not near bottom wall
				// sense bottom grid and if possible further sensing
				setIsExplored( 21-arena.getRobotY(),arena.getRobotX()-1);
				if (isObstacle[21-arena.getRobotY()][arena.getRobotX()-1]!=1 && (arena.getRobotY()-1!=1) )// not obstacle
					setIsExplored(22-arena.getRobotY() , arena.getRobotX()-1);
			}
		}

		else{


			if (latestMessageFromRpi.charAt(1)=='0'){
				setObstacleSensedByRobot(21-arena.getRobotY(),arena.getRobotX()-1,1, 0);
				setIsExplored(21-arena.getRobotY(),arena.getRobotX()-1);

			}
			else if (latestMessageFromRpi.charAt(1)=='1'){
				setObstacleSensedByRobot(21-arena.getRobotY(),arena.getRobotX()-1,0, 1);
				setIsExplored(21-arena.getRobotY(),arena.getRobotX()-1);
				setObstacleSensedByRobot(22-arena.getRobotY(),arena.getRobotX()-1,1, 1);
				setIsExplored(22-arena.getRobotY(),arena.getRobotX()-1);


			}
			else{
				setObstacleSensedByRobot(21-arena.getRobotY(),arena.getRobotX()-1,0, 9);
				setIsExplored(21-arena.getRobotY(),arena.getRobotX()-1);
				setObstacleSensedByRobot(22-arena.getRobotY(),arena.getRobotX()-1,0, 9);
				setIsExplored(22-arena.getRobotY(),arena.getRobotX()-1);

			}



			if (latestMessageFromRpi.charAt(2)=='0'){
				setObstacleSensedByRobot(20-arena.getRobotY(),arena.getRobotX()-2,1, 0);
				setIsExplored(20-arena.getRobotY(),arena.getRobotX()-2);

			}
			else if (latestMessageFromRpi.charAt(2)=='1'){
				setObstacleSensedByRobot(20-arena.getRobotY(),arena.getRobotX()-2,0, 1);
				setIsExplored(20-arena.getRobotY(),arena.getRobotX()-2);
				setObstacleSensedByRobot(20-arena.getRobotY(),arena.getRobotX()-3,1, 1);
				setIsExplored(20-arena.getRobotY(),arena.getRobotX()-3);


			}

			else{
				setObstacleSensedByRobot(20-arena.getRobotY(),arena.getRobotX()-2,0, 9);
				setIsExplored(20-arena.getRobotY(),arena.getRobotX()-2);
				setObstacleSensedByRobot(20-arena.getRobotY(),arena.getRobotX()-3,0, 9);
				setIsExplored(20-arena.getRobotY(),arena.getRobotX()-3);

			}



			if (latestMessageFromRpi.charAt(3)=='0'){
				setObstacleSensedByRobot(19-arena.getRobotY(),arena.getRobotX()-2,1, 0);
				setIsExplored(19-arena.getRobotY(),arena.getRobotX()-2);

			}
			else if (latestMessageFromRpi.charAt(3)=='1'){
				setObstacleSensedByRobot(19-arena.getRobotY(),arena.getRobotX()-2,0, 1);
				setIsExplored(19-arena.getRobotY(),arena.getRobotX()-2);
				setObstacleSensedByRobot(19-arena.getRobotY(),arena.getRobotX()-3,1, 1);
				setIsExplored(19-arena.getRobotY(),arena.getRobotX()-3);


			}

			else{
				setObstacleSensedByRobot(19-arena.getRobotY(),arena.getRobotX()-2,0, 9);
				setIsExplored(19-arena.getRobotY(),arena.getRobotX()-2);
				setObstacleSensedByRobot(19-arena.getRobotY(),arena.getRobotX()-3,0, 9);
				setIsExplored(19-arena.getRobotY(),arena.getRobotX()-3);

			}


			if (latestMessageFromRpi.charAt(4)=='0'){
				setObstacleSensedByRobot(18-arena.getRobotY(),arena.getRobotX()-2,1, 0);
				setIsExplored(18-arena.getRobotY(),arena.getRobotX()-2);

			}
			else if (latestMessageFromRpi.charAt(4)=='1'){
				setObstacleSensedByRobot(18-arena.getRobotY(),arena.getRobotX()-2,0, 1);
				setIsExplored(18-arena.getRobotY(),arena.getRobotX()-2);
				setObstacleSensedByRobot(18-arena.getRobotY(),arena.getRobotX()-3,1, 1);
				setIsExplored(18-arena.getRobotY(),arena.getRobotX()-3);


			}

			else{
				setObstacleSensedByRobot(18-arena.getRobotY(),arena.getRobotX()-2,0, 9);
				setIsExplored(18-arena.getRobotY(),arena.getRobotX()-2);
				setObstacleSensedByRobot(18-arena.getRobotY(),arena.getRobotX()-3,0, 9);
				setIsExplored(18-arena.getRobotY(),arena.getRobotX()-3);

			}





			if (latestMessageFromRpi.charAt(5)=='0'){
				setObstacleSensedByRobot(17-arena.getRobotY(),arena.getRobotX()-1,1, 0);
				setIsExplored(17-arena.getRobotY(),arena.getRobotX()-1);

			}
			else if (latestMessageFromRpi.charAt(5)=='1'){
				setObstacleSensedByRobot(17-arena.getRobotY(),arena.getRobotX()-1,0, 1);
				setIsExplored(17-arena.getRobotY(),arena.getRobotX()-1);
				setObstacleSensedByRobot(16-arena.getRobotY(),arena.getRobotX()-1,1, 1);
				setIsExplored(16-arena.getRobotY(),arena.getRobotX()-1);


			}
			else{
				setObstacleSensedByRobot(17-arena.getRobotY(),arena.getRobotX()-1,0, 9);
				setIsExplored(17-arena.getRobotY(),arena.getRobotX()-1);
				setObstacleSensedByRobot(16-arena.getRobotY(),arena.getRobotX()-1,0, 9);
				setIsExplored(16-arena.getRobotY(),arena.getRobotX()-1);

			}


		}
	}



	public void sense(){// update grid that have been explored
		if (arena.getIsRealArena())
			System.out.println("inside sense method ");
		if (arena.getIsRealArena()){
			client.sendMessage('s', 0); //to tell arduino to send me sensing

			latestMessageFromRpi=client.receiveMessage();
			if (latestMessageFromRpi.equals("TIMEOUT")) {
				sense();
				return;
			}
			while(latestMessageFromRpi.charAt(0)!='s'|| latestMessageFromRpi.length()!=6 ){
				//required string is example s10012
				//from left,first letter s mean it is a sensor reading. next letter is reading from first sensor
				//..last letter is reading from fifth sensor
				System.out.println("inside while loop of NOT sensor info string ");

				latestMessageFromRpi=client.receiveMessage();
				if (latestMessageFromRpi.equals("TIMEOUT")) {
					sense();
					return;
				}

			}
			System.out.println("latestMessageFromRpi IS "+latestMessageFromRpi);

		}
		if(arena.getRobotDirection()=="NORTH"){
			northSense();
		}
		else if(arena.getRobotDirection()=="SOUTH"){
			southSense();
		}
		else if(arena.getRobotDirection()=="EAST"){
			eastSense();
		}
		else if(arena.getRobotDirection()=="WEST"){
			westSense();
		}


	}

	boolean stickToWallDone = false;

	public boolean exploreOneStep(){
		// row then column
		
		// get current position and set walkThrough to be true
		walkBefore[18-arena.getRobotY()][arena.getRobotX()-1]=true;
		walkBefore[18-arena.getRobotY()][arena.getRobotX()]=true;
		walkBefore[18-arena.getRobotY()][arena.getRobotX()+1]=true;

		walkBefore[19-arena.getRobotY()][arena.getRobotX()-1]=true;
		walkBefore[19-arena.getRobotY()][arena.getRobotX()]=true;
		walkBefore[19-arena.getRobotY()][arena.getRobotX()+1]=true;

		walkBefore[20-arena.getRobotY()][arena.getRobotX()-1]=true;
		walkBefore[20-arena.getRobotY()][arena.getRobotX()]=true;
		walkBefore[20-arena.getRobotY()][arena.getRobotX()+1]=true;
		
		
		
		
		
		boolean res = false;
		if (stickToWallDone) {
//			res = carryOutConfusionMode();
			return true;
		} else {
			stickToWallDone = stickToWall();
			if (stickToWallDone) {
				return true;
			}
		}
		
		//walkBefore
		
		System.out.println("Robot position: " + arena.getRobotX() + ", " + arena.getRobotY());
		
		if (arena.getIsRealArena()) {
			shouldResetCalib = true;
			calibrate();
		}

		sense();
		
		if (arena.getIsRealArena()) {
			shouldResetCalib = false;
			calibrate();
		}

		// need add in condition: when certain condition then i call calibrate() method
		
		shouldResetCalib = true;
		return res;
	}

	
	boolean[] calibDone = {false, false, false}; //0 - left, 1 - forward, 2 - right
	boolean shouldResetCalib = true;
	
	public void calibrate(){
		
		boolean shouldChangeOrder = false;
		
		System.out.println("inside calibrate function");
		if (shouldResetCalib) {
			calibDone[0] = false;
			calibDone[1] = false;
			calibDone[2] = false;
		}
		boolean leftIsWallOrObstacle=false;
		boolean rightIsWallOrObstacle=false;
		boolean frontIsWallOrObstacle=false;

		if(arena.getRobotDirection().equals("NORTH")){
			if(arena.getRobotX()==1 || obstacleSensedByRobot[19 - arena.getRobotY()][arena.getRobotX()-2]==1 &&
					obstacleSensedByRobot[19 - arena.getRobotY() - 1][arena.getRobotX()-2]==1 &&
					obstacleSensedByRobot[19 - arena.getRobotY() + 1][arena.getRobotX()-2]==1)
				leftIsWallOrObstacle=true;
			if(arena.getRobotX()==13 || obstacleSensedByRobot[19 - arena.getRobotY()][arena.getRobotX()+2]==1 &&
					obstacleSensedByRobot[19 - arena.getRobotY() - 1][arena.getRobotX()+2]==1 &&
					obstacleSensedByRobot[19 - arena.getRobotY() + 1][arena.getRobotX()+2]==1)
				rightIsWallOrObstacle=true;
			if(arena.getRobotY()==18 || obstacleSensedByRobot[19 - arena.getRobotY()- 2][arena.getRobotX()-1]==1
					&& obstacleSensedByRobot[19 - arena.getRobotY()- 2][arena.getRobotX()]==1
							&& obstacleSensedByRobot[19 - arena .getRobotY()- 2][arena.getRobotX()+1]==1)
				frontIsWallOrObstacle=true;
		}

		else if(arena.getRobotDirection().equals("SOUTH")){
			if(arena.getRobotX()==1 || obstacleSensedByRobot[19 - arena.getRobotY()][arena.getRobotX()-2]==1 &&
					obstacleSensedByRobot[19 - arena.getRobotY() - 1][arena.getRobotX()-2]==1 &&
					obstacleSensedByRobot[19 - arena.getRobotY() + 1][arena.getRobotX()-2]==1)
				rightIsWallOrObstacle=true;
			if(arena.getRobotX()==13 || obstacleSensedByRobot[19 - arena.getRobotY()][arena.getRobotX()+2]==1 &&
					obstacleSensedByRobot[19 - arena.getRobotY() - 1][arena.getRobotX()+2]==1 &&
					obstacleSensedByRobot[19 - arena.getRobotY() + 1][arena.getRobotX()+2]==1)
				leftIsWallOrObstacle=true;
			if(arena.getRobotY()==1 || obstacleSensedByRobot[19 - arena.getRobotY() + 2][arena.getRobotX()-1]==1
					 && obstacleSensedByRobot[19 - arena.getRobotY() + 2][arena.getRobotX()]==1
					 && obstacleSensedByRobot[19 - arena.getRobotY() + 2][arena.getRobotX()+1]==1)
						frontIsWallOrObstacle=true;
		}


		else if(arena.getRobotDirection().equals("EAST")){
			if(arena.getRobotY()==18 || obstacleSensedByRobot[19 - arena.getRobotY() - 2][arena.getRobotX()]==1 &&
					obstacleSensedByRobot[19 - arena.getRobotY() - 2][arena.getRobotX() - 1]==1 &&
					obstacleSensedByRobot[19 - arena.getRobotY() - 2][arena.getRobotX() + 1]==1)
				leftIsWallOrObstacle=true;
			if(arena.getRobotY()==1 || obstacleSensedByRobot[19 - arena.getRobotY() + 2][arena.getRobotX()]==1 &&
					obstacleSensedByRobot[19 - arena.getRobotY() + 2][arena.getRobotX() - 1]==1 &&
					obstacleSensedByRobot[19 - arena.getRobotY() + 2][arena.getRobotX() + 1]==1)
				rightIsWallOrObstacle=true;
			if(arena.getRobotX()==13 || obstacleSensedByRobot[19 - arena.getRobotY()][arena.getRobotX()+2]==1
					 && obstacleSensedByRobot[19 - arena.getRobotY() - 1][arena.getRobotX()+2]==1
					 && obstacleSensedByRobot[19 - arena.getRobotY() + 1][arena.getRobotX()+2]==1)
						frontIsWallOrObstacle=true;
		}


		else if(arena.getRobotDirection().equals("WEST")){
			if(arena.getRobotY()==18 || obstacleSensedByRobot[19 - arena.getRobotY() - 2][arena.getRobotX()]==1 &&
					obstacleSensedByRobot[19 - arena.getRobotY() - 2][arena.getRobotX() + 1]==1 &&
					obstacleSensedByRobot[19 - arena.getRobotY() - 2][arena.getRobotX() - 1]==1)
				rightIsWallOrObstacle=true;
			if(arena.getRobotY()==1 || obstacleSensedByRobot[19 - arena.getRobotY() + 2][arena.getRobotX()]==1 &&
					obstacleSensedByRobot[19 - arena.getRobotY() + 2][arena.getRobotX() + 1]==1 &&
					obstacleSensedByRobot[19 - arena.getRobotY() + 2][arena.getRobotX() - 1]==1)
				leftIsWallOrObstacle=true;
			if(arena.getRobotX()==1 || obstacleSensedByRobot[19 - arena.getRobotY()][arena.getRobotX()-2]==1
					 || obstacleSensedByRobot[19 - arena.getRobotY() - 1][arena.getRobotX()-2]==1
					 || obstacleSensedByRobot[19 - arena.getRobotY() + 1][arena.getRobotX()-2]==1)
						frontIsWallOrObstacle=true;
		}

		if   ( !   (leftIsWallOrObstacle || rightIsWallOrObstacle|| frontIsWallOrObstacle)   )
			return;
		
		if (frontIsWallOrObstacle && leftIsWallOrObstacle &&
				rightIsWallOrObstacle ) {
			rightIsWallOrObstacle = false;
		}
			

		else if (frontIsWallOrObstacle && leftIsWallOrObstacle ) {
			
		} else if (frontIsWallOrObstacle && rightIsWallOrObstacle ) {
			// change calibration order
			shouldChangeOrder = true;
			
		} else if (rightIsWallOrObstacle && leftIsWallOrObstacle ) {
			rightIsWallOrObstacle = false;
		} else {
			if(arena.getIsRealArena()) {
				if ( arena.getNumOfTotalMovement() <= 2 )
				 return;

			}
		}

		if(leftIsWallOrObstacle && !calibDone[0]){// wall or obstacle at left
			client.sendMessage('c', 0);
			latestMessageFromRpi=client.receiveMessage();
			// keep listening until rpie say start cd
			while(  !(     (latestMessageFromRpi.equals("cf"))  ||   (latestMessageFromRpi.equals("cs") )    ))     {
				if (latestMessageFromRpi.equals("TIMEOUT")) {
					client.sendMessage('c', 0);
				}
				System.out.println("enter while loop of message !=cf or cd ");
				latestMessageFromRpi=client.receiveMessage();
			}

			if(latestMessageFromRpi.equals("cs")) {
				arena.resetNumOfTotalMovement();
				calibDone[0] = true;
			}

		}
		if (frontIsWallOrObstacle && !calibDone[1] && !shouldChangeOrder){// wall or obstacle at front
			client.sendMessage('c', 2);
			latestMessageFromRpi=client.receiveMessage();
			// keep listening until rpie say start cd
			while(  !(     (latestMessageFromRpi.equals("cf"))  ||   (latestMessageFromRpi.equals("cs") )    ))     {
				if (latestMessageFromRpi.equals("TIMEOUT")) {
					client.sendMessage('c', 2);
				}
				System.out.println("enter while loop of message !=cf or cd ");
				latestMessageFromRpi=client.receiveMessage();
			}

			if(latestMessageFromRpi.equals("cs")) {
				arena.resetNumOfTotalMovement();
				calibDone[1] = true;
			}

		}
		if (rightIsWallOrObstacle && !calibDone[2]){	// wall or obstacle at right
			client.sendMessage('c', 1);
			latestMessageFromRpi=client.receiveMessage();
			// keep listening until rpie say start cd
			while(  !(     (latestMessageFromRpi.equals("cf"))  ||   (latestMessageFromRpi.equals("cs") )    ))     {
				if (latestMessageFromRpi.equals("TIMEOUT")) {
					client.sendMessage('c', 1);
				}
				System.out.println("enter while loop of message !=cf or cd ");
				latestMessageFromRpi=client.receiveMessage();
			}

			if(latestMessageFromRpi.equals("cs")) {
				arena.resetNumOfTotalMovement();
				calibDone[2] = true;
			}

		}
		if (frontIsWallOrObstacle && !calibDone[1] && shouldChangeOrder){// wall or obstacle at front
			client.sendMessage('c', 2);
			latestMessageFromRpi=client.receiveMessage();
			// keep listening until rpie say start cd
			while(  !(     (latestMessageFromRpi.equals("cf"))  ||   (latestMessageFromRpi.equals("cs") )    ))     {
				if (latestMessageFromRpi.equals("TIMEOUT")) {
					client.sendMessage('c', 2);
				}
				System.out.println("enter while loop of message !=cf or cd ");
				latestMessageFromRpi=client.receiveMessage();
			}

			if(latestMessageFromRpi.equals("cs")) {
				arena.resetNumOfTotalMovement();
				calibDone[1] = true;
			}

		}



	}

	public void initialCalibrate(){
		arena.turnRight();
		arena.turnRight();
		if (arena.getIsRealArena())
			client.sendMessage('c', 2);
		arena.turnRight();
		if (arena.getIsRealArena())
			client.sendMessage('c', 2);
		arena.resetNumOfTotalMovement();
		arena.turnRight();
	}




	public void explore() {//
		//all direction of arena below are
		//real north south east west
		// regardless of robot direction

		// robot current initial position is considered as explored
		// i assume initial robot position is such that the 8 grid surrounding it is free space

		if (arena.getIsRealArena()==true){
			setObstacleSensedByRobot(18-arena.getRobotY(),arena.getRobotX()-1,0, 0);
			setObstacleSensedByRobot(18-arena.getRobotY(),arena.getRobotX(),0, 0);
			setObstacleSensedByRobot(18-arena.getRobotY(),arena.getRobotX()+1,0, 0);

			setObstacleSensedByRobot(19-arena.getRobotY(),arena.getRobotX()-1,0, 0);
			setObstacleSensedByRobot(19-arena.getRobotY(),arena.getRobotX(),0, 0);
			setObstacleSensedByRobot(19-arena.getRobotY(),arena.getRobotX()+1,0, 0);

			setObstacleSensedByRobot(20-arena.getRobotY(),arena.getRobotX()-1,0, 0);
			setObstacleSensedByRobot(20-arena.getRobotY(),arena.getRobotX(),0, 0);
			setObstacleSensedByRobot(20-arena.getRobotY(),arena.getRobotX()+1,0, 0);
		}


		setIsExplored(18-arena.getRobotY(),arena.getRobotX()-1);
		setIsExplored(18-arena.getRobotY(),arena.getRobotX());
		setIsExplored(18-arena.getRobotY(),arena.getRobotX()+1);

		setIsExplored(19-arena.getRobotY(),arena.getRobotX()-1);
		setIsExplored(19-arena.getRobotY(),arena.getRobotX());
		setIsExplored(19-arena.getRobotY(),arena.getRobotX()+1);

		setIsExplored(20-arena.getRobotY(),arena.getRobotX()-1);
		setIsExplored(20-arena.getRobotY(),arena.getRobotX());
		setIsExplored(20-arena.getRobotY(),arena.getRobotX()+1);


		setStartZoneAsExploredAndNoObstacle();

		// f1 mean tell arduino forward one step and then send me sensor reading
		// F1 mean tell arduino  forward one step that all
		// same for backward , turn left or turn right eg b1,B1,l1,L1,r1,R1


		Thread animateThread = new Thread(new Runnable() {
			public void run() {
				long timeInitial = System.currentTimeMillis();

				double timeForOneStepInSec=1/  ((double)arena.getStepPerSec());
				long timeForOneStepInMs=(long) (timeForOneStepInSec*1000);

				boolean exploreComplete = false;

				if(arena.getIsRealArena()){
					 latestMessageFromRpi=client.receiveMessage();
						// keep listening until rpie say start ic
						while(!    (latestMessageFromRpi.equals("ic"))){
							System.out.println("enter while loop of message !=ic ");
							latestMessageFromRpi=client.receiveMessage();
						}


					initialCalibrate();


					latestMessageFromRpi=client.receiveMessage();
					// keep listening until rpie say start explore
					while(!    (latestMessageFromRpi.equals("se"))){
						System.out.println("enter while loop of message !=se ");
						latestMessageFromRpi=client.receiveMessage();
					}
				}
				sense();
				while(  !exploreComplete ){

					System.out.println("noOfGridExplored:"+noOfGridExplored);
					//System.out.println("currentCoveragePercentage:"+currentCoveragePercentage);

					if (exploreByPercentageExplored&&
							(currentCoveragePercentage>= targetCoveragePercentage) )
						break;
					if (exploreByTimeLimit){
						long currentTime = System.currentTimeMillis();
						if ((currentTime - timeInitial >= timeLimit*1000)){
							System.out.println("Time up");
							break;
						}
						int seconds=(int) (timeLimit-((currentTime - timeInitial)/1000));
						System.out.println(""+seconds+" seconds left");

					}// end of if exploreByTimeLimit

					exploreComplete = exploreOneStep();
					if (arena.getIsRealArena()==false){
						try {
							Thread.sleep( timeForOneStepInMs);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				}// end of while
				
				
				System.out.println("SHOUT EXPLORATION COMPLETED");
				
				fastModeActivated=true;

				
				createExploredMapDescriptor();
				createObstacleMapDescriptor();
				
				
				// make sure facing north then calibrate then shortest path
				
				
				if(arena.getIsRealArena()){
					if(arena.getRobotDirection()=="SOUTH")
						client.sendMessage('r',2);
					else if (arena.getRobotDirection()=="EAST")
						client.sendMessage('l',1);
					else if (arena.getRobotDirection()=="WEST")
						client.sendMessage('r',1);
				}
				arena.setRobotDirection("NORTH");
				
				if (arena.getIsRealArena())
					calibrate();
				
				// for some very WEIRD reason maybe due to multithreading, i need to wait for 2sec
				// after reaching start zone before i can start fastest path
				// it is ok to wait for 2sec since the robot need time to calibrate after reaching start zone
				// and professot need take photo of android simulator map and this will take more than 8sec for sure
				// and we have 1min to prepare for fastest path

				//if (arena.getIsRealArena())
				//	calibrate();

				try {
					Thread.sleep(2000);
				} catch (Exception e) {
					e.printStackTrace();
				}
				System.out.println("Safe to carry out fastest path");

				//have move back to start position after exploration completed
				// now wait for rpie tell me start shortest path
				if (arena.getIsRealArena()){
					latestMessageFromRpi=client.receiveMessage();
					while(! latestMessageFromRpi.equals("ssp") ){
						System.out.println("inside while loop of NOT shortest path call string ");
						latestMessageFromRpi=client.receiveMessage();
					}

				}

				arena.findShortestPath();


			}// end of run
		});// end of thread

		animateThread.start();


	}


	public void createExploredMapDescriptor(){
		try{
			String binaryString="";
			String hexString="";
			String temporaryBinaryString="";
			PrintWriter writer = new PrintWriter("ExploredMapDescriptor.txt", "UTF-8");
			writer.println("11");
			binaryString+="11";
			for (int i=HEIGHT-1; i>-1; i--) {// 0 to height -1
				for (int j = 0; j < WIDTH; j++) {
					int myInt = (getIsExplored()[i][j]) ? 1 : 0;
					binaryString+=myInt;
					if (j==WIDTH-1)
						writer.println(""+myInt);
					else
						writer.print(""+myInt);
				}// end of for j
			}// end of for i
			writer.println("11");
			binaryString+="11";
			writer.close();


			for (int i=0;i<binaryString.length()+1;i++){
				if (i%4==0&&i!=0){
					int decimal = Integer.parseInt(temporaryBinaryString,2);
					hexString += Integer.toString(decimal,16);
					temporaryBinaryString="";
				}
				if (i==binaryString.length())
					break;
				temporaryBinaryString+=binaryString.charAt(i);

			}

			if (arena.getIsRealArena())
				client.sendFullString("{ \"explore\" : \" "+hexString+ " \" }");

			System.out.println("I have sent Android ExploredMapDescriptorInHex:" +hexString);
			
			PrintWriter hexWriter = new PrintWriter("ExploredMapDescriptorInHex.txt", "UTF-8");
			hexWriter.print(hexString);
			hexWriter.close();


		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}//end of createExploredMapDescriptor()

	public void createObstacleMapDescriptor(){

		try{
			String binaryString="";
			String hexString="";
			String temporaryBinaryString="";

			int numOfBit=0;
			PrintWriter writer = new PrintWriter("ObstacleMapDescriptor.txt", "UTF-8");
			for (int i=HEIGHT-1; i>-1; i--) {
				boolean writeOnNewLine=true;
				for (int j = 0; j < WIDTH; j++) {
					if (getIsExplored()[i][j]==true&& obstacleSensedByRobot[i][j]==1)
					{
						if (writeOnNewLine){
							writer.println("");
							writer.print("1");
						}
						else
							writer.print("1");
						numOfBit++;
						writeOnNewLine=false;
						binaryString+=1;
					}

					else if (getIsExplored()[i][j]==true&& obstacleSensedByRobot[i][j]==0)
					{
						if (writeOnNewLine){
							writer.println("");
							writer.print("0");
						}
						else
							writer.print("0");
						numOfBit++;
						writeOnNewLine=false;
						binaryString+=0;
					}

				}// end of for j
			}// end of for i

			if(numOfBit%8!=0){// not multiple of 8
				int numOfBitForPadding=8-(numOfBit%8);

				writer.println("");
				for(int i=0; i<numOfBitForPadding; i++){
					writer.print("0");
					binaryString+=0;

				}

			}

			writer.close();


			for (int i=0;i<binaryString.length()+1;i++){
				if (i%4==0&&i!=0){
					int decimal = Integer.parseInt(temporaryBinaryString,2);
					hexString += Integer.toString(decimal,16);
					temporaryBinaryString="";
				}
				if (i==binaryString.length())
					break;
				temporaryBinaryString+=binaryString.charAt(i);

			}

			if (arena.getIsRealArena())
				client.sendFullString("{ \"block \" : \""+hexString+ "\" }");
			
			
			System.out.println("I have sent Android ObstacleMapDescriptorInHex:" +hexString);

			PrintWriter hexWriter = new PrintWriter("ObstacleMapDescriptorInHex.txt", "UTF-8");
			hexWriter.print(hexString);
			hexWriter.close();


		}// end of try
		catch(Exception e) {
			e.printStackTrace();
		}
	}// end of createObstacleMapDescriptor()

	LinkedList<int[]> path = new LinkedList<>();
	int[] prevCoord;
	String prevDirection;

	private boolean carryOutConfusionMode(){
		int[] prevCoordCopy = prevCoord == null ? null : Arrays.copyOf(prevCoord, 2);
		boolean[][] obstaclesCopy = transformObstacleMap();
		int x = arena.getRobotX(), y = arena.getRobotY();
		String direction = arena.getRobotDirection();
		boolean shouldTurnLeft = false, shouldTurnRight = false;

		boolean runUnexploredPart = false;

		if (!path.isEmpty()) {
			int[] coord = path.pollFirst();
			System.out.println("Inside non empty path " + Arrays.toString(prevCoord) + " " + Arrays.toString(coord) + " " + direction);

			if (isValidRobotPosition(obstaclesCopy, coord[0], coord[1])) {
		 		if (direction.equals("NORTH")) {
		 			if (prevCoord[0] < coord[0]) shouldTurnRight = true;
		 			else if (prevCoord[0] > coord[0]) shouldTurnLeft = true;

				} else if (direction.equals("SOUTH")) {
					if (prevCoord[0] < coord[0]) shouldTurnLeft = true;
					else if (prevCoord[0] > coord[0]) shouldTurnRight = true;

				} else if (direction.equals("EAST")) {
					if (prevCoord[1] < coord[1]) shouldTurnLeft = true;
					else if (prevCoord[1] > coord[1]) shouldTurnRight = true;

				} else if (direction.equals("WEST")) {
					if (prevCoord[1] < coord[1]) shouldTurnRight = true;
					else if (prevCoord[1] > coord[1]) shouldTurnLeft = true;

				}
		 		if (shouldTurnLeft) {
		 			arena.turnLeft();
		 			path.addFirst(coord);

	 			} else if (shouldTurnRight) {
	 				arena.turnRight();
	 				path.addFirst(coord);

	 			} else {
	 				if (prevCoord[1] < coord[1]) {
	 					if (direction.equals("NORTH")) arena.moveForward();
	 					else {
	 						arena.turnRight();
 							arena.turnRight();
 							path.addFirst(coord);
 							coord = prevCoord;
	 					}
	 				} else if (prevCoord[1] > coord[1]) {
	 					if (direction.equals("SOUTH")) arena.moveForward();
	 					else {
	 						arena.turnRight();
 							arena.turnRight();
 							path.addFirst(coord);
 							coord = prevCoord;
	 					}
	 				} else if (prevCoord[0] < coord[0]) {
	 					if (direction.equals("EAST")) arena.moveForward();
	 					else {
	 						arena.turnRight();
 							arena.turnRight();
 							path.addFirst(coord);
 							coord = prevCoord;
	 					}
	 				} else if (prevCoord[0] > coord[0]) {
	 					if (direction.equals("WEST")) arena.moveForward();
	 					else {
	 						arena.turnRight();
 							arena.turnRight();
 							path.addFirst(coord);
 							coord = prevCoord;
	 					}
	 				}
	 				setEnteredGoalZone(coord);
//	 				arena.setRobotPosition(coord[0], coord[1]);
	 				prevCoord = coord;
	 			}
			} else {
				path.clear();
				runUnexploredPart = true;
			}
		} else {
			runUnexploredPart = true;
		}

		if (runUnexploredPart) {
			PriorityQueue<CoordWithDistance> pq = getClosestUnexploredVertex();
			while(!pq.isEmpty()) {
				CoordWithDistance coo = pq.poll();
				boolean[][] obstacleMap = transformObstacleMap();

				System.out.println("coo " + coo.x + "  " + coo.y + " " + coo.recur);
				if(!isValidRobotPosition(obstacleMap, coo.x, coo.y)) {
					System.out.println("Continuing " + coo.x + " " + coo.y + " " + coo.recur);

					// New stategy
					if (coo.recur) {
						addToPQ(pq, coo.x, coo.y, coo.x - 1, coo.y);
						addToPQ(pq, coo.x, coo.y, coo.x - 1, coo.y + 1);
						addToPQ(pq, coo.x, coo.y, coo.x, coo.y + 1);
						addToPQ(pq, coo.x, coo.y, coo.x + 1, coo.y + 1);
						addToPQ(pq, coo.x, coo.y, coo.x + 1, coo.y);
						addToPQ(pq, coo.x, coo.y, coo.x + 1, coo.y - 1);
						addToPQ(pq, coo.x, coo.y, coo.x, coo.y - 1);
						addToPQ(pq, coo.x, coo.y, coo.x - 1, coo.y - 1);
					}


					// Top left corner
//					if (coo.x == 0 && coo.y == HEIGHT - 1) {
//						CoordWithDistance secPoint = new CoordWithDistance();
//						secPoint.x = 1;
//						secPoint.y = HEIGHT - 2;
//						secPoint.distance = manhattanDistance(x, y, secPoint.x, secPoint.y);
//						pq.add(secPoint);
//					} else if (coo.x == 0) { // Left edge
//						CoordWithDistance secPoint = new CoordWithDistance();
//						secPoint.x = 1;
//						secPoint.y = coo.y;
//						secPoint.distance = manhattanDistance(x, y, secPoint.x, secPoint.y);
//						pq.add(secPoint);
//					} else if (coo.y == HEIGHT - 1 && coo.x == WIDTH - 1) { // Top right corner
//						CoordWithDistance secPoint = new CoordWithDistance();
//						secPoint.x = WIDTH - 2;
//						secPoint.y = HEIGHT - 2;
//						secPoint.distance = manhattanDistance(x, y, secPoint.x, secPoint.y);
//						pq.add(secPoint);
//					} else if (coo.y == HEIGHT - 1) { // Top edge
//						CoordWithDistance secPoint = new CoordWithDistance();
//						secPoint.x = coo.x;
//						secPoint.y = HEIGHT - 2;
//						secPoint.distance = manhattanDistance(x, y, secPoint.x, secPoint.y);
//						pq.add(secPoint);
//					} else if (coo.x == WIDTH - 1 && coo.y == 0) { // Bottom right corner
//						CoordWithDistance secPoint = new CoordWithDistance();
//						secPoint.x = WIDTH - 2;
//						secPoint.y = 1;
//						secPoint.distance = manhattanDistance(x, y, secPoint.x, secPoint.y);
//						pq.add(secPoint);
//					} else if (coo.x == WIDTH - 1) { // Right edge
//						CoordWithDistance secPoint = new CoordWithDistance();
//						secPoint.x = WIDTH - 2;
//						secPoint.y = coo.y;
//						if (isValidRobotPosition(obstacleMap, secPoint.x, secPoint.y)) {
//
//						} else if (isValidRobotPosition(obstacleMap, secPoint.x, secPoint.y - 1)) {
//							secPoint.y = secPoint.y - 1;
//						} else if (isValidRobotPosition(obstacleMap, secPoint.x, secPoint.y + 1)) {
//							secPoint.y = secPoint.y + 1;
//						}
//						secPoint.distance = manhattanDistance(x, y, secPoint.x, secPoint.y);
//						pq.add(secPoint);
//					} else if (coo.y == 0) { // Bottom edge
//						CoordWithDistance secPoint = new CoordWithDistance();
//						secPoint.x = coo.x;
//						secPoint.y = 1;
//						secPoint.distance = manhattanDistance(x, y, secPoint.x, secPoint.y);
//						pq.add(secPoint);
//					}

					continue;
				}

				path = (new FastestPath(arena, obstacleMap)).
						fastestExpandedPath(x, y, coo.x, coo.y);

				prevCoord = path.pollFirst();
				int[] coord = path.pollFirst();

				System.out.println("prev: " + prevCoord[0] + " " + prevCoord[1]);

				if (coord != null && isValidRobotPosition(obstaclesCopy, coord[0], coord[1])) {
			 		if (direction.equals("NORTH")) {
			 			if (prevCoord[0] < coord[0]) shouldTurnRight = true;
			 			else if (prevCoord[0] > coord[0]) shouldTurnLeft = true;

					} else if (direction.equals("SOUTH")) {
						if (prevCoord[0] < coord[0]) shouldTurnLeft = true;
						else if (prevCoord[0] > coord[0]) shouldTurnRight = true;

					} else if (direction.equals("EAST")) {
						if (prevCoord[1] < coord[1]) shouldTurnLeft = true;
						else if (prevCoord[1] > coord[1]) shouldTurnRight = true;

					} else if (direction.equals("WEST")) {
						if (prevCoord[1] < coord[1]) shouldTurnRight = true;
						else if (prevCoord[1] > coord[1]) shouldTurnLeft = true;

					}
			 		if (shouldTurnLeft) {
			 			arena.turnLeft();
			 			path.addFirst(coord);

		 			} else if (shouldTurnRight) {
		 				arena.turnRight();
		 				path.addFirst(coord);

		 			} else {
		 				if (prevCoord[1] < coord[1]) {
		 					if (direction.equals("NORTH")) arena.moveForward();
		 					else {
		 						arena.turnRight();
	 							arena.turnRight();
	 							path.addFirst(coord);
	 							coord = prevCoord;
		 					}
		 				} else if (prevCoord[1] > coord[1]) {
		 					if (direction.equals("SOUTH")) arena.moveForward();
		 					else {
		 						arena.turnRight();
	 							arena.turnRight();
	 							path.addFirst(coord);
	 							coord = prevCoord;
		 					}
		 				} else if (prevCoord[0] < coord[0]) {
		 					if (direction.equals("EAST")) arena.moveForward();
		 					else {
		 						arena.turnRight();
	 							arena.turnRight();
	 							path.addFirst(coord);
	 							coord = prevCoord;
		 					}
		 				} else if (prevCoord[0] > coord[0]) {
		 					if (direction.equals("WEST")) arena.moveForward();
		 					else {
		 						arena.turnRight();
	 							arena.turnRight();
	 							path.addFirst(coord);
	 							coord = prevCoord;
		 					}
		 				}
		 				setEnteredGoalZone(coord);
//		 				arena.setRobotPosition(coord[0], coord[1]);
		 				prevCoord = coord;
		 			}
					break;
				}
			}
		}

		System.out.println("End of confuse method:");
		System.out.println(arena.getRobotX());
		System.out.println(arena.getRobotY());
		System.out.println(arena.getRobotDirection());
		System.out.println("Done End of confuse method:");

		if (prevCoordCopy != null && prevDirection != null && prevCoordCopy[0] == arena.getRobotX() &&
				prevCoordCopy[1] == arena.getRobotY() && prevDirection.equals(arena.getRobotDirection())) {
			System.out.println(Arrays.toString(prevCoordCopy));
			System.out.println(prevDirection);
			System.out.println("Robot has stopped");
			return true;
		}
		prevDirection = arena.getRobotDirection();


		return false;


//		int x = arena.getRobotX(), y = arena.getRobotY();
//		String direction = arena.getRobotDirection();
//
//		boolean shouldTurnLeft = false, shouldTurnRight = false;
//
//		boolean[][] obstacleMap = transformObstacleMap();
//		boolean[][] obstaclesCopy = transformObstacleMap();
//		int[] endPoint = getClosestUnexploredVertex();
//		makePointValid(obstacleMap, HEIGHT - 1 - endPoint[1], endPoint[0]);
//
//		System.out.println(x + " " + y + " " + endPoint[0] + " " + endPoint[1]);
// 		LinkedList<int[]> path = (new FastestPath(arena, obstacleMap)).
//				fastestExpandedPath(x, y, endPoint[0], endPoint[1]);
//
// 		int[] prevCoord = path.pollFirst();
// 		int[] coord = path.pollFirst();
//
// 		System.out.println(Arrays.toString(prevCoord));
// 		System.out.println(Arrays.toString(coord));
//
// 		if (direction.equals("NORTH")) {
//			if (prevCoord[0] < coord[0]) shouldTurnRight = true;
//			else if (prevCoord[0] > coord[0]) shouldTurnLeft = true;
//
//		} else if (direction.equals("SOUTH")) {
//			if (prevCoord[0] < coord[0]) shouldTurnLeft = true;
//			else if (prevCoord[0] > coord[0]) shouldTurnRight = true;
//
//		} else if (direction.equals("EAST")) {
//			if (prevCoord[1] < coord[1]) shouldTurnLeft = true;
//			else if (prevCoord[1] > coord[1]) shouldTurnRight = true;
//
//		} else if (direction.equals("WEST")) {
//			if (prevCoord[1] < coord[1]) shouldTurnRight = true;
//			else if (prevCoord[1] > coord[1]) shouldTurnLeft = true;
//
//		}
//
// 		if (isValidRobotPosition(obstaclesCopy, coord[0], coord[1])) {
// 	 		if (shouldTurnLeft) {
// 				arena.turnLeft();
// 			} else if (shouldTurnRight) {
// 				arena.turnRight();
// 			} else {
// 				arena.setRobotPosition(coord[0], coord[1]);
// 			}
// 		} else {
//
// 			if (!shouldTurnLeft && !shouldTurnRight) {
// 				arena.turnRight();
// 			} else if (shouldTurnLeft || shouldTurnRight) {
// 				arena.moveForward();
// 			}
//
//// 			int northCount = getUnexploredCount(x - 1, 0, 3, HEIGHT - 2 - y);
//// 			int southCount = getUnexploredCount(x - 1, HEIGHT - y - 1, 3, y + 1);
//// 			int westCount = getUnexploredCount(0, HEIGHT - y - 2, x, 3);
//// 			int eastCount = getUnexploredCount(x + 2, HEIGHT - y -2, WIDTH - x - 2, 3);
////
//// 			if (direction.equals("NORTH")) {
////
////
//// 			} else if (direction.equals("SOUTH")) {
//// 				if (prevCoord[0] < coord[0]) shouldTurnLeft = true;
//// 				else if (prevCoord[0] > coord[0]) shouldTurnRight = true;
////
//// 			} else if (direction.equals("EAST")) {
//// 				if (prevCoord[1] < coord[1]) shouldTurnLeft = true;
//// 				else if (prevCoord[1] > coord[1]) shouldTurnRight = true;
////
//// 			} else if (direction.equals("WEST")) {
//// 				if (prevCoord[1] < coord[1]) shouldTurnRight = true;
//// 				else if (prevCoord[1] > coord[1]) shouldTurnLeft = true;
////
//// 			}
// 		}

	}

	private void setEnteredGoalZone(int[] coord) {
		if (coord[0] == 13 && coord[1] == 18) enteredGoalZone = true;
	}


	boolean twoRightsInProgress = false;
	boolean wallStart = true;
	boolean wallGoForward = false;
	int stickToWallCount = 0;

	int leftTurnCount = 0;

	private boolean stickToWall() {
		stickToWallCount++;
		if (arena.getRobotX() == 1 && arena.getRobotY() == 1 && !wallStart
				&& stickToWallCount != 1) {
			return true;
		}
		wallStart = false;
		if (twoRightsInProgress) {
			arena.turnRight();
			twoRightsInProgress = false;
		}else if (wallGoForward) {
			arena.moveForward();
			wallGoForward = false;
		} else {
			if (leftTurnCount >= 4) {
				// stuck in a loop
				System.out.println("Stuck in a loop");
				if (obstacleDirectlyAhead()) {
					arena.turnRight();
					leftTurnCount = 0;
					return false;
				}
			}
			if (wallCanGoLeft()) {
				arena.turnLeft();
				leftTurnCount++;
				wallGoForward = true;
			} else {
				if (wallCanGoForward()) {
					arena.moveForward();
				} else if (wallCanGoRight()) {
					arena.turnRight();
					wallGoForward = true;
				} else {
					arena.turnRight();
					twoRightsInProgress = true;
				}
				leftTurnCount = 0;
			}
		}
		return false;
	}

	private boolean obstacleDirectlyAhead() {
		int col = arena.getRobotX();
		int row = 19 - arena.getRobotY();

		return obstacleSensedByRobot[row][col - 1] == 1 || obstacleSensedByRobot[row][col] == 1 ||
				obstacleSensedByRobot[row][col + 1] == 1;
	}

	private boolean wallCanGoLeft() {
//		int x = arena.getRobotX(), y = arena.getRobotY();
//		String direction = arena.getRobotDirection();
//		int newY = HEIGHT - 1 - y;
//
//		if (direction.equals("NORTH")) {
//			if (x == 1) return false;
//			else {
//				if (obstacleSensedByRobot[newY][x - 2] == 1 || obstacleSensedByRobot[newY - 1][x - 2] == 1 ||
//						obstacleSensedByRobot[newY + 1][x - 2] == 1)
//					return false;
//			}
//		} else if (direction.equals("EAST")) {
//			if (y == HEIGHT - 2) return false;
//			else {
//				if (obstacleSensedByRobot[newY - 2][x - 1] == 1 || obstacleSensedByRobot[newY - 2][x] == 1 ||
//						obstacleSensedByRobot[newY - 2][x + 1] == 1)
//					return false;
//			}
//		} else if (direction.equals("SOUTH")) {
//			if (x == WIDTH - 2) return false;
//			else {
//				if (obstacleSensedByRobot[newY][x + 2] == 1 || obstacleSensedByRobot[newY - 1][x + 2] == 1 ||
//						obstacleSensedByRobot[newY + 1][x + 2] == 1)
//					return false;
//			}
//		} else {
//			if (y == 1) return false;
//			else {
//				if (obstacleSensedByRobot[newY + 2][x - 1] == 1 || obstacleSensedByRobot[newY + 2][x] == 1 ||
//						obstacleSensedByRobot[newY + 2][x + 1] == 1)
//					return false;
//			}
//		}
//
//		return true;
		boolean[][] obstacleMap = transformObstacleMapWithUnknown();
		int x = arena.getRobotX(), y = arena.getRobotY();
		String direction = arena.getRobotDirection();

		if (direction.equals("NORTH")) {
			return isValidRobotPosition(obstacleMap, x - 1, y);
		} else if (direction.equals("EAST")) {
			return isValidRobotPosition(obstacleMap, x, y + 1);
		} else if (direction.equals("SOUTH")) {
			return isValidRobotPosition(obstacleMap, x + 1, y);
		} else {
			return isValidRobotPosition(obstacleMap, x, y - 1);
		}
	}

	private boolean wallCanGoForward() {
		boolean[][] obstacleMap = transformObstacleMapWithUnknown();
		int x = arena.getRobotX(), y = arena.getRobotY();
		String direction = arena.getRobotDirection();

		if (direction.equals("NORTH")) {
			return isValidRobotPosition(obstacleMap, x, y + 1);
		} else if (direction.equals("EAST")) {
			return isValidRobotPosition(obstacleMap, x + 1, y);
		} else if (direction.equals("SOUTH")) {
			return isValidRobotPosition(obstacleMap, x, y - 1);
		} else {
			return isValidRobotPosition(obstacleMap, x - 1, y);
		}
	}

	private boolean wallCanGoRight() {
//		int x = arena.getRobotX(), y = arena.getRobotY();
//		String direction = arena.getRobotDirection();
//		int newY = HEIGHT - 1 - y;
//
//		if (direction.equals("NORTH")) {
//			if (x == WIDTH - 2) return false;
//			else {
//				if (obstacleSensedByRobot[newY][x + 2] == 1 || obstacleSensedByRobot[newY - 1][x + 2] == 1 ||
//						obstacleSensedByRobot[newY + 1][x + 2] == 1)
//					return false;
//			}
//		} else if (direction.equals("EAST")) {
//			if (y == 1) return false;
//			else {
//				if (obstacleSensedByRobot[newY + 2][x - 1] == 1 || obstacleSensedByRobot[newY + 2][x] == 1 ||
//						obstacleSensedByRobot[newY + 2][x + 1] == 1)
//					return false;
//			}
//		} else if (direction.equals("SOUTH")) {
//			if (x == 1) return false;
//			else {
//				if (obstacleSensedByRobot[newY][x - 2] == 1 || obstacleSensedByRobot[newY - 1][x - 2] == 1 ||
//						obstacleSensedByRobot[newY + 1][x - 2] == 1)
//					return false;
//			}
//		} else {
//			if (y == HEIGHT - 2) return false;
//			else {
//				if (obstacleSensedByRobot[newY - 2][x - 1] == 1 || obstacleSensedByRobot[newY - 2][x] == 1 ||
//						obstacleSensedByRobot[newY - 2][x + 1] == 1)
//					return false;
//			}
//		}
//
//		return true;
		boolean[][] obstacleMap = transformObstacleMapWithUnknown();
		int x = arena.getRobotX(), y = arena.getRobotY();
		String direction = arena.getRobotDirection();

		if (direction.equals("NORTH")) {
			return isValidRobotPosition(obstacleMap, x + 1, y);
		} else if (direction.equals("EAST")) {
			return isValidRobotPosition(obstacleMap, x, y - 1);
		} else if (direction.equals("SOUTH")) {
			return isValidRobotPosition(obstacleMap, x - 1, y);
		} else {
			return isValidRobotPosition(obstacleMap, x, y + 1);
		}
	}

	private boolean isValidRobotPosition(boolean[][] obstacles, int x, int y) {
		if (x < 1 || x > WIDTH - 2 || y < 1 || y > HEIGHT - 2) return false;
		else {
			int newY = HEIGHT - y - 1;

			return !(obstacles[newY][x] || obstacles[newY - 1][x - 1] || obstacles[newY - 1][x] ||
					obstacles[newY - 1][x + 1] || obstacles[newY][x + 1] || obstacles[newY + 1][x + 1] ||
					obstacles[newY + 1][x] || obstacles[newY + 1][x - 1] || obstacles[newY][x - 1]);
//
//			return !(obstacles[newY][x] || obstacles[newY + 1][x] ||
//					obstacles[newY + 1][x - 1] || obstacles[newY][x - 1]);
		}
	}

	private boolean[][] transformObstacleMap() {
		boolean[][] result = new boolean[HEIGHT][WIDTH];
		for (int i = 0; i < HEIGHT; i++) {
			for (int j = 0; j < WIDTH; j++) {
				if (obstacleSensedByRobot[i][j] == 1)
					result[i][j] = true;
				else
					result[i][j] = false;
			}
		}
		return result;
	}

	public boolean[][] transformObstacleMapWithUnknown() {
		boolean[][] result = new boolean[HEIGHT][WIDTH];
		for (int i = 0; i < HEIGHT; i++) {
			for (int j = 0; j < WIDTH; j++) {
				if (obstacleSensedByRobot[i][j] == 0)
					result[i][j] = false;
				else
					result[i][j] = true;
			}
		}
		return result;
	}

	boolean enteredGoalZone = true;

	private PriorityQueue<CoordWithDistance> getClosestUnexploredVertex() {
		PriorityQueue<CoordWithDistance> pq = new PriorityQueue<>();

		for (int i = 0; i < HEIGHT; i++) {
			for (int j = 0; j < WIDTH; j++) {
				if (i == 0 && j == WIDTH - 2) {
					if (!enteredGoalZone) {
						CoordWithDistance coo = new CoordWithDistance();
						int newY = HEIGHT - 1 - i;
						coo.x = j;
						coo.y = newY;
						coo.distance = manhattanDistance(j, newY, arena.getRobotX(), arena.getRobotY());
						pq.add(coo);
					}
				} else if (obstacleSensedByRobot[i][j] == 2) {
					int newY = HEIGHT - 1 - i;
					int dist = manhattanDistance(j, newY, arena.getRobotX(), arena.getRobotY());
					CoordWithDistance coo = new CoordWithDistance();
					coo.x = j;
					coo.y = newY;
					coo.distance = dist;

					pq.add(coo);
				}
			}
		}
		return pq;
	}

	private void makePointValid(boolean[][] obstacles, int row, int col) {
		unsetObstacle(obstacles, row, col);
		unsetObstacle(obstacles, row - 1, col - 1);
		unsetObstacle(obstacles, row - 1, col);
		unsetObstacle(obstacles, row - 1, col + 1);
		unsetObstacle(obstacles, row, col + 1);
		unsetObstacle(obstacles, row + 1, col + 1);
		unsetObstacle(obstacles, row + 1, col);
		unsetObstacle(obstacles, row + 1, col - 1);
		unsetObstacle(obstacles, row, col - 1);
	}

	private void unsetObstacle(boolean[][] obstacles, int row, int col) {
		if (col < 0 || col > WIDTH - 1 || row < 0 || row > HEIGHT - 1) return;
		else obstacles[row][col] = false;
	}

	private void moveToStartPosition(){
		if (arena.getRobotX() == 1 && arena.getRobotY() == 1) return;
		FastestPath fastestPath = new FastestPath(arena, transformObstacleMapWithUnknown());
		LinkedList<int[]> pathToStart = fastestPath.
			fastestExpandedPath(arena.getRobotX(), arena.getRobotY(), 1, 1);

		fastestPath.moveRobot(pathToStart);
	}

	private int manhattanDistance(int x1, int y1, int x2, int y2) {
		return Math.abs(x1 - x2) + Math.abs(y1 - y2);
	}

	private boolean isLeftMoreUnexplored() {
		int x = arena.getRobotX(), y = HEIGHT - 1 - arena.getRobotY();
		int numLeft = 0, numRight = 0;
		for (int i = 0; i < x - 1; i++) {
			if (obstacleSensedByRobot[y - 1][i] == 2) numLeft++;
			if (obstacleSensedByRobot[y][i] == 2) numLeft++;
			if (obstacleSensedByRobot[y + 1][i] == 2) numLeft++;
		}

		for (int i = x + 2; i < WIDTH; i++) {
			if (obstacleSensedByRobot[y - 1][i] == 2) numRight++;
			if (obstacleSensedByRobot[y][i] == 2) numRight++;
			if (obstacleSensedByRobot[y + 1][i] == 2) numRight++;
		}
		return numLeft > numRight;
//		String direction = arena.getRobotDirection();
//		int x = arena.getRobotX(), y = HEIGHT - 1 - arena.getRobotY();
//		if (direction.equals("NORTH")) {
//			if (getUnexploredPercentage(0, 0, x + 1, HEIGHT) > getUnexploredPercentage(x, 0, WIDTH - x, HEIGHT))
//				return true;
//			else
//				return false;
//		} else if (direction.equals("SOUTH")) {
//			if (getUnexploredPercentage(0, 0, x + 1, HEIGHT) > getUnexploredPercentage(x, 0, WIDTH - x, HEIGHT))
//				return false;
//			else
//				return true;
//		} else if (direction.equals("EAST")) {
//			if (getUnexploredPercentage(0, 0, WIDTH, y) > getUnexploredPercentage(0, y, WIDTH, HEIGHT - y))
//				return true;
//			else
//				return false;
//		} else {
//			if (getUnexploredPercentage(0, 0, WIDTH, y) > getUnexploredPercentage(0, y, WIDTH, HEIGHT - y))
//				return false;
//			else
//				return true;
//		}
	}

	private void addToPQ(PriorityQueue<CoordWithDistance> pq, int sX, int sY, int eX, int eY) {
		if (eX < 1 || eX > WIDTH - 2 || eY < 1 || eY > HEIGHT - 2) {
			return;
		} else if (obstacleSensedByRobot[HEIGHT - 1 - eY][eX] != 1){
			CoordWithDistance secPoint = new CoordWithDistance();
			secPoint.x = eX;
			secPoint.y = eY;
			secPoint.distance = manhattanDistance(sX, sY, eX, eY);
			secPoint.recur = false;
			System.out.println("add to pq: " + eX + " " + eY);
			pq.add(secPoint);
		}
	}

	private int getUnexploredCount(int x, int y, int width, int height) {
		int num = 0;
		for (int i = y; i < y + height; i++) {
			for (int j = x; j < x + width; j++) {
				if (obstacleSensedByRobot[i][j] == 2) num++;
			}
		}
		return num;
	}

	class CoordWithDistance implements Comparable<CoordWithDistance>{
		int x, y;
		int distance;
		boolean recur = true;

		@Override
		public int compareTo(CoordWithDistance o) {
			// TODO Auto-generated method stub
			return this.distance - o.distance;
		}


	}


}
