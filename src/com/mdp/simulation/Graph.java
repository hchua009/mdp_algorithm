package com.mdp.simulation;

import java.util.Arrays;
import java.util.LinkedList;

public class Graph {
	
	EdgeNode[] nodes;
	int numVertices;
	
	Graph(int numVertices) {
		this.numVertices = numVertices;
		nodes = new EdgeNode[numVertices];
		for (int i = 0; i < numVertices; i++) nodes[i] = new EdgeNode();
	}
	
	public void addEdge(int x, int y, double weight) {
		nodes[x].edges.add(new Edge(y, weight));
	}
	
	public void changeWeight(int x, int y, double newWeight) {
		for (Edge edge : nodes[x].edges) {
			if (edge.y == y) {
				edge.weight = newWeight;
				break;
			}
		}
	}
	
	LinkedList<Integer> getShortestPath(int start, int end) {
		boolean[] inTree = new boolean[numVertices];
		double[] distance = new double[numVertices];
		int[] parent = new int[numVertices];
		
		Arrays.fill(parent, -1);
		Arrays.fill(distance, Integer.MAX_VALUE);
		distance[start] = 0;
		
		int node = start;
		
		while(!inTree[node]) {
			inTree[node] = true;
			
			for (Edge edge : nodes[node].edges) {
				int y = edge.y;
				double weight = edge.weight;
				if(!inTree[y] && (distance[y] > distance[node] + weight)) {
					distance[y] = distance[node] + weight;
					parent[y] = node;
				}
			}
			
			double min = Double.MAX_VALUE;
			for (int i = 0; i < numVertices; i++) {
				if (!inTree[i] && distance[i] < min) {
					min = distance[i];
					node = i;
				}
			}
		}
		
		LinkedList<Integer> path = new LinkedList<>();
		node = end;
		while (node != -1) {
			path.addFirst(node);
			node = parent[node];
		}
		
		return path;
	}

}

class EdgeNode {
	LinkedList<Edge> edges = new LinkedList<>();
}

class Edge {
	int y;
	double weight;
	
	Edge(int y, double weight) {
		this.y = y;
		this.weight = weight;
	}
}
