package com.mdp.simulation;

public interface ControlsListener {
	
	void explore();
	void findShortestPath();
	void resetMap();
	void setStepPerSec(int stepPerSec);
	void setPercentageExplored(int percentage);
	void setTimeLimit(int timeLimit);

}
