package com.mdp.simulation;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class ControlsDisplay extends JPanel {
	
	private ControlsListener controlsListener;
	
	ControlsDisplay(ControlsListener controlsListener) {
		this.setBackground(Color.GREEN);
		Box vertical = Box.createVerticalBox();		
		this.controlsListener = controlsListener;
		
		JButton loadMapButton = new JButton("Load/Reset Map");
		loadMapButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				controlsListener.resetMap();
			}
		});
		
		JButton exploreButton = new JButton("Explore");
		exploreButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				controlsListener.explore();
			}
		});
		
		JButton shortestPathButton = new JButton("Shortest Path");
		shortestPathButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				controlsListener.findShortestPath();
			}
		});
		
		JLabel stepLabel=new JLabel("Number of step per second");
		 String[] stepChoices = {"1 step","2 step", "3 step"};

		 final JComboBox<String> speed = new JComboBox<String>(stepChoices);
		 
		 speed.addActionListener(new ActionListener() {				
				@Override
				public void actionPerformed(ActionEvent e) {
					if(speed.getSelectedItem().toString().equals("1 step"))
						controlsListener.setStepPerSec(1);
					else if(speed.getSelectedItem().toString().equals("2 step"))
						controlsListener.setStepPerSec(2);
					else if(speed.getSelectedItem().toString().equals("3 step"))
						controlsListener.setStepPerSec(3);			
				}
			});
		
		 
		 JLabel timeLabel=new JLabel("Exloration time limit");
		 String[] timeChoices = {"1 min 00sec","1 min 30sec", "2 min 00sec"};

		 final JComboBox<String> time = new JComboBox<String>(timeChoices);
		 time.addActionListener(new ActionListener() {				
				@Override
				public void actionPerformed(ActionEvent e) {
					if(time.getSelectedItem().toString().equals("1 min 00sec"))
						controlsListener.setTimeLimit(60);
					else if(time.getSelectedItem().toString().equals("1 min 30sec"))
						controlsListener.setTimeLimit(90);
					else if(time.getSelectedItem().toString().equals("2 min 00sec"))
						controlsListener.setTimeLimit(120);		
				}
			});
		 
		 
		 
		 
		 JLabel percentageLabel=new JLabel("coverage % required ");
		 String[] percentageChoices = {"40%","80%", "100%", "no % limit"};

		 final JComboBox<String> percentage = new JComboBox<String>(percentageChoices);
		 percentage.addActionListener(new ActionListener() {				
				@Override
				public void actionPerformed(ActionEvent e) {
					if(percentage.getSelectedItem().toString().equals("40%"))
						controlsListener.setPercentageExplored(40);
					else if(percentage.getSelectedItem().toString().equals("80%"))
						controlsListener.setPercentageExplored(80);
					else if(percentage.getSelectedItem().toString().equals("100%"))
						controlsListener.setPercentageExplored(100);		
				}
			});
		 
		 
	
		 vertical.add(loadMapButton);
		 vertical.add(exploreButton);
		 vertical.add(shortestPathButton);
		 vertical.add(stepLabel);
		 vertical.add(speed);
		 vertical.add(timeLabel);
		 vertical.add(time);
		 vertical.add(percentageLabel);
		 vertical.add(percentage);
		 this.add(vertical);
	}
	
	

}
