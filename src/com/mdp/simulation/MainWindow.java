package com.mdp.simulation;

import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

public class MainWindow extends JFrame {
	
	private Arena arena;
	private ControlsDisplay controlsDisplay;
	
	MainWindow() {
		this.setTitle("Simulator");
		this.setLayout(new GridLayout(1, 2));
		
		arena = new Arena();
//		arena.loadAndUpdateMap();
		//if (arena.getIsRealArena()==false)
			controlsDisplay = new ControlsDisplay(arena);
		
		this.add(arena);
		//if (arena.getIsRealArena()==false)
			this.add(controlsDisplay);
		
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setExtendedState(JFrame.MAXIMIZED_BOTH);
		this.pack();
		this.setVisible(true);
		
	}

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			
			@Override
			public void run() {
				displayFrame();
			}
		});
	}
	
	private static void displayFrame() {
		new MainWindow();
	}

}
